<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ButtonClick extends Model
{
    protected $table = 'button_click';

    protected $fillable = [
      'info', 'user_id'
    ];
}
