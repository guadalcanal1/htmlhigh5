<?php

namespace App\Console;

use App\Game;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\User;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call(function(){
            User::updateAllPercentiles();
            Game::updateAllStats();
        })->daily();
        $schedule->call(function(){
            foreach(Game::all() as $game){
                $game->updateScoreDistribution();
            }
        })->hourly();

        $schedule->command('backup:run --only-db')->daily();
        $schedule->command('backup:run')->weekly();
        $schedule->command('backup:clean')->weekly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
