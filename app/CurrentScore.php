<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrentScore extends Model
{
    protected $table = 'current_score';
    protected $fillable = ['score', 'user_id', 'game_id', 'timestamp', 'updated_at', 'created_at', 'ip'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function game(){
        return $this->belongsTo('App\Game');
    }
}
