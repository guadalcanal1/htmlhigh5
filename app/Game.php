<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Game extends Model
{
    protected $fillable = [
        'name',
        'thumbnail',
        'cover_image_id',
        'slug',
        'genre_id',
        'description',
        'keywords'
    ];

    public function scoreDistribution(){
        return $this->hasOne('App\ScoreDistribution');
    }

    public function scores(){
        return $this->hasMany('App\Score');
    }

    public function genre(){
        return $this->belongsTo('App\GameGenre');
    }

    public function currentScores(){
        return $this->hasMany('App\CurrentScore');
    }

    public function trophies(){
        return $this->hasMany('App\Trophy')->orderBy('sort_value', 'asc');
    }

    public function ratings(){
        return $this->hasMany('App\Rating');
    }

    public function timeSpent(){
        return $this->hasMany('App\TimeSpent');
    }

    public function timeSpentString(){
        $seconds = $this->timeSpent()->sum('seconds');
        $days = floor($seconds / 60 / 60 / 24);
        return $days . ':' . gmdate("H:i:s", $seconds);
    }

    public function images(){
        return $this->hasMany('App\Image');
    }

    public function coverImage(){
        return Image::find($this->cover_image_id);
    }

    public function getCoverImageIdAttribute($val){
        return Image::find($val);
    }

    public function getThumbnailAttribute($val){
        return Image::find($val);
    }

    public function recentScores(){
        $lastWeek = \Carbon\Carbon::today()->subDays(7);
        $recentScores = $this->scores()->where('created_at', '>=', date($lastWeek))->orderBy('value', 'asc')->get();
        return $recentScores;
    }

    public function gameSetting(){
        return $this->hasOne('App\GameSetting');
    }

    public function topScores($number){
        return $this->allUserHighScores()->take($number)->get();
    }

    public function allHighScores(){
        return $this->scores()->groupBy('user_id')->select(DB::raw('MAX(value) as value, user_id'))->orderBy('value', 'desc');
    }

    public function allUserHighScores(){
        return $this->scores()->where('user_id', '>', '0')->groupBy('user_id')->select(DB::raw('MAX(value) as value, user_id'))->orderBy('value', 'desc');
    }

    public static function updateAllStats(){
        $games = Game::all();
        GameStat::truncate();
        foreach($games as $game){
            $rating = $game->ratings()->avg('value');
            if(!$rating)
                $rating = 0;
            $plays = $game->scores()->get()->count();
            $timeSpent = $game->timeSpent()->sum('seconds');
            GameStat::create([
                'game_id' => $game->id,
                'rating' => $rating,
                'play_count' => $plays,
                'time_spent' => $timeSpent
            ]);
        }
    }

    public function updateScoreDistribution(){
        dump('WE ARE UPDATING IT');
        $allScores = $this->scores()->orderBy('value', 'asc')->get();

        $scores = collect(DB::select("select * from `scores` ".
            "where value > (select * from(select AVG(value)-2.5*STDDEV(value) from `scores` where game_id = $this->id) x)" ."
         and `game_id` = $this->id and value < (select * from(select AVG(value)+2.5*STDDEV(value) from scores where game_id = $this->id) x)" ."
          order by value asc"));
        $distinctScores =$this->scores()->distinct('value')->count('value');
        $noPlays = !$scores->count();
        $playCount = !$noPlays ? $allScores->count() : 0;

        $lowestScore = !$noPlays ? $scores->first()->value : 0;
        $highestScore = !$noPlays ? $scores->last()->value : 0;
        $numSets = !$noPlays ? min(max(20, min(min(50, $playCount/50), $distinctScores)), $playCount) : 0;
        $increment = !$noPlays ? ($highestScore - $lowestScore) / $numSets : 0;
        $increment=ceil($increment);
        $incrementLessOne = $increment-1;
        if($increment > 1) {
            $scoreGroups = collect(DB::select("select concat($increment*floor(`value`/$increment), '-', $increment*floor(`value`/$increment) + $incrementLessOne) as `range`,
        count(*) as `count` from scores where `value` >= $lowestScore and `value` <= $highestScore and `game_id` = $this->id group by 1 order by `value`;"));
        } else {
            $scoreGroups = collect(DB::select("select concat($increment*floor(`value`/$increment)) as `range`,
        count(*) as `count` from scores where `value` >= $lowestScore and `value` <= $highestScore and `game_id` = $this->id group by 1 order by `value`;"));
        }
        $previous = ScoreDistribution::where('game_id', $this->id)->first();
        if($previous){
            $previous->save([
                'score_data' => json_encode($scoreGroups)
            ]);
        }else {
            ScoreDistribution::create([
                'game_id' => $this->id,
                'score_data' => json_encode($scoreGroups)
            ]);
        }
        return $scoreGroups;
    }
}
