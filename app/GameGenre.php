<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameGenre extends Model
{
    protected $table = 'game_genre';

    public function game(){
        return $this->belongsToMany('App\Game');
    }
}
