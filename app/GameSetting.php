<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameSetting extends Model
{
    protected $fillable = [
        'game_id', 'max_score', 'max_score_increment', 'score_increment_time', 'max_time_spent'
    ];

    public function game(){
        return $this->belongsTo('App\Game');
    }
}
