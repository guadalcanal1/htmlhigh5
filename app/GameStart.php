<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameStart extends Model
{
    protected $fillable = ['user_id', 'game_id'];
}
