<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameStat extends Model
{
    protected $fillable = [
      'game_id', 'rating', 'time_spent', 'play_count'
    ];
}
