<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GotchaPlayer extends Model
{
    protected $fillable = ['name', 'dorm', 'more_info', 'is_waldock', 'alive', 'target', 'targeted_by'];

    public function getTargetAttribute($target){
        return GotchaPlayer::where('id', '=', $target)->first();
    }

    public function getTargetedByAttribute($targetedBy){
        return GotchaPlayer::where('id', '=', $targetedBy)->first();
    }

    public function killedBy(GotchaPlayer $killer){
        if($killer){
            if($this->target) {
                $this->target->update([
                    'targeted_by' => $killer->id
                ]);
                $killer->update([
                    'target' => $this->target->id
                ]);
            }
        }
        $this->update([
            'alive' => false,
            'target' => 0,
            'targetedBy' => 0
        ]);
    }

    public function chain(){
        $chain = [];
        if($this->targeted_by && $this->targeted_by->is_waldock)
            return $chain;
        if(!$this->target || !$this->target->is_waldock)
            return $chain;
        array_push($chain, $this);
        array_push($chain, $this->target);
        $child = $this->target;
        $length = 0;
        while($child){
            if($child->target && $child->target->is_waldock) {
                $length++;
                array_push($chain, $child->target);
            }
            $child = $child->target;
        }
        return $chain;
    }
}
