<?php

namespace App\Http\Controllers;

use App\ButtonClick;
use App\Game;
use App\GameStart;
use App\IP;
use App\Jobs\SendContactEmail;
use App\Jobs\SendNewGameEmail;
use App\Jobs\SendNewLookEmail;
use App\PageLoad;
use App\PlayPageLoad;
use App\Score;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    private $timeUnits = ['minute' => 0, 'hour' => 1, 'day' => 2, 'week' => 3, 'month' => 4];

    public function index(){
        if(AdminController::isAdmin())
            return view('admin.index');
        return view('index');
    }

    public function createGame(){
        if(AdminController::isAdmin())
            return view('admin.createGame');
        return view('index');
    }

    public function updateGame(){
        if(AdminController::isAdmin())
            return view('admin.updateGame');
        return view('index');
    }

    public function switchView(Request $request){
        $count = $request->get('count');
        $unit = $request->get('unit');
        return redirect("admin/stats/$count/$unit");
    }

    public function viewMetrics($count, $unit){
        $scores = $this->getCountOverTime($count, $this->timeUnits[$unit], Score::class);
        $userScores = $this->getUserCountOverTime($count, $this->timeUnits[$unit], Score::class);
        $signUps = $this->getCountOverTime($count, $this->timeUnits[$unit], User::class);
        $buttonClicks = $this->getCountOverTime($count, $this->timeUnits[$unit], ButtonClick::class);
        $pageLoads = $this->getCountOverTime($count, $this->timeUnits[$unit], PageLoad::class);
        $playPageLoads = $this->getCountOverTime($count, $this->timeUnits[$unit], PlayPageLoad::class);
        $userPageLoads = $this->getUserCountOverTime($count, $this->timeUnits[$unit], PageLoad::class);
        $recentPlays = $this->getRecentPlays();
        $recentPages = $this->getRecentPages();
        $gameStarts = $this->getCountOverTime($count, $this->timeUnits[$unit], GameStart::class);
        if(AdminController::isAdmin())
            return view('admin.viewMetrics', [
                'userLoads' => $userPageLoads,
                'scores' => $scores,
                'userScores' => $userScores,
                'signUps' => $signUps,
                'buttonClicks' => $buttonClicks,
                'pageLoads' => $pageLoads,
                'playLoads' => $playPageLoads,
                'recentPlays' => $recentPlays,
                'recentPages' => $recentPages,
                'gameStarts' => $gameStarts,
                'dailyPlays' => $this->getDailyPlayDistribution()
            ]);
        return abort(404);
    }

    private function getDailyPlayDistribution(){
        $scores = DB::table('scores')->select(DB::raw('game_id, count(game_id) as count'))->whereRaw('Date(created_at) = CURDATE()')->groupBy('game_id')->get();
        foreach($scores as $score){
            $score->game_id = Game::find($score->game_id)->name;
        }
        return json_encode($scores);
    }

    private function getCountOverTime($count, $unit, $model){
        for($num = 0; $num < $count; $num++){
            $scores[$num] = 0;
        }
        $prevCount = 0;
        for($num = 0; $num < $count; $num++){
            $date = Carbon::today();
            switch($unit){
                case $this->timeUnits['minute']: $date = Carbon::now()->subMinutes($num); break;
                case $this->timeUnits['hour']: $date = Carbon::now()->subHours($num); break;
                case $this->timeUnits['day']: $date = Carbon::today()->subDays($num); break;
                case $this->timeUnits['week']: $date = Carbon::today()->subWeeks($num); break;
                case $this->timeUnits['month']: $date = Carbon::today()->subMonths($num); break;
            }
            $scores[$count - 1 - $num] = $model::where('created_at', '>=', $date)->count() - $prevCount;
            $prevCount += $scores[$count - 1 - $num];
        }
        return $scores;
    }

    private function getUserCountOverTime($count, $unit, $model){
        for($num = 0; $num < $count; $num++){
            $scores[$num] = 0;
        }
        $prevCount = 0;
        for($num = 0; $num < $count; $num++){
            $date = Carbon::today();
            switch($unit){
                case $this->timeUnits['minute']: $date = Carbon::now()->subMinutes($num); break;
                case $this->timeUnits['hour']: $date = Carbon::now()->subHours($num); break;
                case $this->timeUnits['day']: $date = Carbon::today()->subDays($num); break;
                case $this->timeUnits['week']: $date = Carbon::today()->subWeeks($num); break;
                case $this->timeUnits['month']: $date = Carbon::today()->subMonths($num); break;
            }
            $scores[$count - 1 - $num] = $model::where('created_at', '>=', $date)->where('user_id', '>', 0)->count() - $prevCount;
            $prevCount += $scores[$count - 1 - $num];
        }
        return $scores;
    }

    private function getRecentPages(){
        $count = 20;
        $ret[0] = "";
        $pages = PageLoad::orderBy('created_at', 'desc')->limit($count)->get();
        for($num = 0; $num < $count; $num++){
            if($pages[$num])
                $ret[$num] = $pages[$num]->info;
            else
                $ret[$num] = "";
        }
        return $ret;
    }

    private function getRecentPlays(){
        $count = 20;
        $ret[0] = "";
        $scores = Score::orderBy('created_at', 'desc')->limit($count)->get();
        for($num = 0; $num < $count; $num++){
            if($scores[$num])
                $ret[$num] = Game::find($scores[$num]->game_id)->name;
            else
                $ret[$num] = "";
        }
        return $ret;
    }

    public function createTrophy(){
        if(AdminController::isAdmin())
            return view('admin.createTrophy');
        return view('index');
    }

    public static function isAdmin(){
        if(Auth::user()){
            if(Auth::user()->is_admin){
                return true;
            }else{
                //Attempted unauthorized access
            }
        }else{
            //Not Logged in
        }
        return false;
    }

    public function contact(Request $request){
        $data = $request->all();
        $this->validate($request, [
            'email' => 'required|string|email|max:255',
            'message' => 'required|string|max:2000',
            'g-recaptcha-response' => 'required|recaptcha'
        ]);
        dispatch(new SendContactEmail($data['email'], $data['message']));
        return redirect('contactSent');
    }

    public function registerButtonClick(Request $request, $slug){
        echo "YOU SUCK";
        var_dump($request);
        $userID = UserController::getUserID();
        ButtonClick::create([
            'user_id' => $userID,
            'info' => $slug
        ]);
        AdminController::setUserIP($request, $userID);
    }

    public static function setUserIP(Request $request, $userID){
        $ip = $request->ip();
        $existingIP = IP::where('user_id', '=', $userID)->where('address', '=', $ip)->first();
        if(!$existingIP)
            IP::create([
                'address'=>$ip,
                'user_id' => $userID
            ]);
        else {
            $otherUser = IP::where('address', '=', $ip)->first();
            if($otherUser){
                $otherUser->update(['user_id'=>$userID]);
            } else
                $existingIP->touch();
        }
    }

    public function sendTestEmail(){
        $user = Auth::user();
        Mail::send('emails.welcome', ['user' => Auth::user()], function ($m) use ($user) {
            $m->from('contact@htmlhigh5.com', 'HTMLHigh5');

            $m->to('wailord284@gmail.com', 'DAT BOI')->subject('Welcome!');
        });
        echo "Mail sent";
    }

    public function notifyUsers(){
        $user = User::where('id', '=', 1)->first();
        $game = Game::where('id', '=', 1)->first();
        dispatch(new SendNewGameEmail($user, $game));
    }
}
