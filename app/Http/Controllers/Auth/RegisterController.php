<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Jobs\SendVerificationEmail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|alpha_dash|string|max:24|unique:users|min:4',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $slug = str_slug($data['name']);
        $i = 1;
        while(User::where('slug', '=', $slug)->first())
            $slug = $slug . $i++;
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'slug' => $slug,
            'email_token' => base64_encode(bcrypt(base64_encode($data['email'])))
        ]);
        return $user;
    }
    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        RegisterController::sendVerificationEmail($user);
        return view('auth.verify', [
            'email' => $user->email
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param $token
     * @return \Illuminate\Http\Response
     */
    public function verify($token)
    {
        $user = User::where('email_token', $token)->first();
        if($user && $user->verified == 0) {
            $user->verified = 1;
            if ($user->save())
                Auth::login($user);
        }
        return view('auth.emailconfirm');
    }

    public static function sendVerificationEmail($user){
        dispatch(new SendVerificationEmail($user));
    }

    public function resendEmail(Request $request, $email){
        event(new Registered($user = $this->create($request->all())));
        $user = User::where('email', '=', $email);
        RegisterController::sendVerificationEmail($user);
        return view('auth.verify', [
            'email' => $user->email
        ]);
    }

//    private function sendWelcomeEmail($email, $userName){
//        Mail::send('emails.welcome', ['user' => $userName], function ($m) use ($userName, $email) {
//            $m->from('admin@htmlhigh5.com', 'HTML High 5');
//            $m->to($email, $userName)->subject('Welcome to HTML High 5!');
//        });
//    }

    public function usernameValid(Request $request){
        $username = ['name' => $request->get('value')];
        $validator = Validator::make($username, ['name' => 'required|alpha_dash|string|max:24|unique:users|min:4']);
        return $validator->errors();
    }
    public function emailValid(Request $request){
        $email = ['email' => $request->get('value')];
        $validator = Validator::make($email, ['email' => 'required|string|email|max:255|unique:users']);
        return $validator->errors();;
    }
    public function passwordValid(Request $request){
        $password = ['password' => $request->get('value'), 'password_confirmation' => $request->get('value2')];
        $validator = Validator::make($password, ['password' => 'required|string|min:6|confirmed']);
        return $validator->errors();
    }
}
