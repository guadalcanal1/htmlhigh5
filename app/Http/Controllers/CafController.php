<?php

namespace App\Http\Controllers;

use App\CafLookup;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CafController extends Controller
{
    public function lunch(Request $request){
        return $this->printMeal($request, 3);
    }
    public function dinner(Request $request){
        return $this->printMeal($request, 4);
    }
    public function breakfast(Request $request){
        return $this->printMeal($request, 1);
    }
    public function brunch(Request $request){
        return $this->printMeal($request, 2);
    }

    public function makeAMeal(Request $request){
        $calories = (int)($request->input('cal') ?? 2000);
        $hour = Carbon::now()->hour;
        $day = Carbon::now()->dayOfWeek;
        $meal = 1;
        if($day === 6)
            $meal = 2;
        else{
            if($hour > 9)
                $meal = 3;
            if($hour > 14)
                $meal = 4;
        }
        $site = $this->getSite();
        $mealData = $this->getMeal($site, $meal);
        $menuItems = $this->getMenuItems($site);
        $menuItemList = [];
        foreach($menuItems as $menuItem){
            $menuItemList[$menuItem->id] = $menuItem;
        }
        $caloricItems = [];
        foreach ($mealData->stations as $station) {
            $items = $station->items;
            foreach($items as $item){
                $menuItem = $menuItemList[$item];
                $kcal = (int)$menuItem->nutrition->kcal;
                if($kcal) {
                    $caloricItems[] = $menuItem;
                }
            }
        }
        $prepWeights = ['and' => 4, 'with' => 6, 'mixed with' => 1, 'spread on' => 1];
        $preps = array();
        foreach ($prepWeights as $prepWeight=>$value)
        {
            $preps = array_merge($preps, array_fill(0, $value, $prepWeight));
        }
        $ret =  "Here is your recommended meal: ";
        $recommendedCalories = $calories/3;
        $totalCalories = 0;
        $finalItems = [];
        while($totalCalories < $recommendedCalories){
            $item = $caloricItems[array_rand($caloricItems)];
            $totalCalories += $item->nutrition->kcal;
            $finalItems[] = $item;
        }
        $count = 0;
        foreach($finalItems as $item){
            $measureWord = "";
            $station = preg_replace("/[^a-z]+/", "", mb_strtolower($item->station));
            $station = preg_replace('/strong/', '', $station);
            switch($station){
                case "beverage": $measureWord = 'a cup of'; break;
                case "cereal": $measureWord = 'a bowl of'; break;
            }
            $ret .= "$measureWord <strong>$item->label</strong>";
            $count++;
            if($count < count($finalItems)){
                $prep = $preps[array_rand($preps)];
                $ret .= " $prep ";
            }
        }
        $ret .= ". This will amount to <i>$totalCalories</i> calories. Enjoy!";
        echo $ret;
    }

    public function can_be_string($var) {
        return $var === null || is_scalar($var) || (is_object($var) && method_exists($var, '__toString'));
    }

    public function nextClass(){

    }

    private function getSite(){
        $base = 'https://mastersuniversity.cafebonappetit.com/';
        $post = curl_init();
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($post, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($post, CURLOPT_HEADER, 0);
        curl_setopt($post,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post,CURLOPT_URL,$base);
        curl_setopt($post,CURLOPT_POST,1);
        curl_setopt($post, CURLOPT_FOLLOWLOCATION, True);
        curl_getinfo($post, CURLINFO_HTTP_CODE);
        $curlresponse = curl_exec($post);
        curl_close($post);
        return $curlresponse;
    }

    private function getMenuItems($html){
        preg_match('/Bamco\.menu_items = (\{.*\});/', $html, $matches, PREG_OFFSET_CAPTURE);
        return json_decode($matches[1][0]);
    }

    private function getMeal($html, $number){
        $mealEnglish = 'meal';
        switch($number){
            case 1: $mealEnglish = 'breakfast'; break;
            case 2: $mealEnglish = 'brunch'; break;
            case 3: $mealEnglish = 'lunch'; break;
            case 4: $mealEnglish = 'dinner'; break;
        }
        CafLookup::create([
            'command' => $mealEnglish
        ]);
        if(preg_match('/Bamco\.dayparts\[\'' . $number . '\'\] = (\{.*\});/', $html, $matches, PREG_OFFSET_CAPTURE))
            return json_decode($matches[1][0]);
        return null;
    }

    public function random(Request $request){
        $curlresponse = $this->getSite();
        $menuItems = $this->getMenuItems($curlresponse);
        $allItems = [];
        foreach($menuItems as $item){
            array_push($allItems, $item);
        }
        $item = $allItems[array_rand($allItems)];

        echo "Eat some $item->label";
    }


    public function printMeal(Request $request, $number){
        $curlresponse = $this->getSite();
        $menuItems = $this->getMenuItems($curlresponse);
        $meal = $this->getMeal($curlresponse, $number);
        $menuItemList = [];
        foreach($menuItems as $menuItem){
            $menuItemList[$menuItem->id] = $menuItem;
        }
        if($meal){
            $allItems = [];
            $detailed =  !!$request->input('detailed');
            $insane =  !!$request->input('insane');
            $joins = ['along with some', 'and', 'and finally:', 'and to top it all off:'];

            foreach ($meal->stations as $station) {
                $items = $station->items;
                if (mb_strtolower($station->label) !== 'soup')
                    foreach ($items as $item) {
                        $menuItem = $menuItemList[$item];
                        if (($menuItem->special || $insane) && !strpos(strtolower($menuItem->label), 'pizza')) {
                            $name = $menuItem->label;
                            if($detailed && $menuItem->description)
                                $name .= ' (' . $menuItem->description . ')';
                            array_push($allItems, $name);
                        }
                    }
            }
            $ret = 'We\'ve got ';
            $length = sizeof($allItems);
            for ($i = 0; $i < $length; $i++) {
                if ($i < $length - 1 || $length < 2)
                    $ret .= $allItems[$i] . ', ';
                else
                    $ret .= $joins[array_rand($joins)] . ' ' . $allItems[$i] . '.';
            }

            echo $ret;
        } else {
            echo 'How the heck should I know? Check out HTMLHigh5.com! Check out HTMLHigh5.com! Check out HTMLHigh5.com!';
        }
    }
}
