<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Game;
use App\Percentile;
use App\GameStat;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
        $allGames = new Game();
        $firstGames = $allGames->orderBy('created_at', 'desc')->take(5)->get();
        return view('index', [
            'games' => $firstGames,
            'allGames' => $allGames->orderBy('created_at', 'desc')->get()
        ]);
    }
}
