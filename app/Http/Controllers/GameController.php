<?php

namespace App\Http\Controllers;

use App\CurrentScore;
use App\Game;
use App\GameSetting;
use App\GameStat;
use App\Image;
use App\Jobs\SendNewGameEmail;
use App\Percentile;
use App\Rating;
use App\Score;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('games.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('games.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!AdminController::isAdmin()){
            return redirect('/');
        } else {
            $description = $request->get('description');
            $keywords = $request->get('keywords');
            $genre = $request->get('genre');
            $name = $request->get('name');
            $coverImageFile = $request->file('cover_image');
            $thumbnailImageFile = $request->file('thumbnail_image');

            $file = $request->file('zip');
            //create an instance of ZipArchive Class
            $zip = new \ZipArchive;

            //open the file that you want to unzip.
            //NOTE: give the correct path. In this example zip file is in the same folder
            $zipped = $zip->open($file);
            // get the absolute path to $file, where the files has to be unzipped
            $path = pathinfo(realpath($file), PATHINFO_DIRNAME);
            //check if it is actually a Zip file
            if ($zipped) {
                $coverImage = Image::create([
                    'alt_text' => $name . ' cover image'
                ]);
                $thumbnailImage = Image::create([
                    'alt_text' => $name . ' thumbnail'
                ]);
                $coverImageFile->move(public_path() . '/images/gameImages/' . $coverImage->id . '/', 'original.png');
                $thumbnailImageFile->move(public_path() . '/images/gameImages/' . $thumbnailImage->id . '/', 'original.png');
                $game = Game::create([
                    'name' => $name,
                    'description' => $description,
                    'keywords' => $keywords,
                    'genre_id' => $genre,
                    'cover_image_id' => $coverImage->id,
                    'thumbnail' => $thumbnailImage->id,
                    'slug' => Str::slug($name)
                ]);

                $gameSettings = GameSetting::create([
                    'game_id' => $game->id,
                    'max_score' => $request->get('max_score'),
                    'max_score_increment' => $request->get('max_score_increment'),
                    'score_increment_time' => $request->get('score_increment_time'),
                    'max_time_spent' => $request->get('max_time_spent'),
                ]);
                //if yes then extract it to the said folder
                $extract = $zip->extractTo(public_path() . '/games_code/' . $game->id);
                //if unzipped succesfully then show the success message
                if ($extract) {
                    echo "Your file extracted to $path";
                    $allUsers = User::all();
                    foreach($allUsers as $user) {
                        if($user->is_subscribed)
                            dispatch(new SendNewGameEmail($user, $game));
                    }
                } else {
                    echo "your file not extracted. Path: $path";
                }

                //close the zip
                $zip->close();

                //return redirect('/admin');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $start = round(microtime(true) * 1000);
        $game = Game::where('slug', $slug)->first();
        if(!$game){
            abort(404);
        }
        $timeSpent = $game->timeSpentString();
        $allScores = $game->scores()->orderBy('value', 'asc')->get();
        $noPlays = !$allScores->count();

        $playCount = !$noPlays ? $allScores->count() : 0;
        $averageScore = !$noPlays ? $game->scores()->avg('value') : 0;
        $topScores = !$noPlays ? $game->topScores(10) : 0;

        $rating = !$noPlays ? round($game->ratings()->avg('value')*2)/2 : 0;
        $userRating = Auth::user() ? Rating::where([
            ['game_id', '=', $game->id],
            ['user_id', '=', Auth::user()->id]
        ])->get()->first() : 0;
        $userRating = $userRating ? $userRating->value : 0;
        $keywords = explode(',', $game->keywords);
        $scoreGroups = json_decode($game->scoreDistribution ? $game->scoreDistribution->score_data : null) ?? [];

        return view('games.show', [
            'game' => $game,
            'timeSpent' => $timeSpent,
            'playCount' => $playCount,
            'averageScore' => round($averageScore * 100) / 100,
            'scoreGroups' => $scoreGroups,
            'topScores' => $topScores,
            'rating' => $rating,
            'userRating' => $userRating,
            'numRatings' => $game->ratings()->count(),
            'keywords' => $keywords
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        if(!AdminController::isAdmin())
            return redirect('/');
        $game = Game::where('slug', '=', $slug)->first();
        return view('games.edit', ['game' => $game, 'settings' => $game->gameSetting()->first()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        if(!AdminController::isAdmin())
            return redirect('/');
        $game = Game::where('slug', '=', $slug)->first();
        $description = $request->get('description');
        $keywords = $request->get('keywords');
        $genre_id = $request->get('genre');
        $name = $request->get('name');
        $slug = Str::slug($name);
        $game->update([
            'name' => $name,
            'description' => $description,
            'keywords' => $keywords,
            'genre_id' => $genre_id,
            'slug' => $slug
        ]);

        $gameSettings = $game->gameSetting()->first();
        $gameSettings->update([
            'max_score' => $request->get('max_score'),
            'max_score_increment' => $request->get('max_score_increment'),
            'score_increment_time' => $request->get('score_increment_time'),
            'max_time_spent' => $request->get('max_time_spent'),
        ]);
        return redirect('/admin');
    }

    public function play($slug){
        if($slug == 'popsicio'){
            return redirect('http://popsicio.htmlhigh5.com');
        }else
            return view('games.play', ['game' => Game::where('slug', '=', $slug)->first()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function rate(Request $request){
        if(!Auth::user())
            return 'You must be logged in to rate!';
        $value = $request->get('rating');
        $game = $request->get('game');
        $user = Auth::user()->id;
        Rating::where([
            ['game_id', '=', $game],
            ['user_id', '=', $user]
        ])->delete();
        Rating::create([
            'user_id' => $user,
            'game_id' => $game,
            'value' => $value
        ]);

        return $value;
    }

    public static function validateScore($increment, $timestamp, $gameID, $totalScore, CurrentScore $score, $final){
        echo "VALIDATING<br/>";
        if($gameID === 8)
            return true;
        $game = Game::find($gameID);
        $rules = $game->gameSetting()->first();
        //var_dump($game);
        //var_dump($rules);
        if($increment <= $rules->max_score_increment){
            if($totalScore <= $rules->max_score){
                $secondsSinceUpdate = $score->updated_at->diffInSeconds(Carbon::now());
                $secondsSinceCreate = $score->created_at->diffInSeconds(Carbon::now())-20;
                if($final || $secondsSinceUpdate >= $rules->score_increment_time){
                    if($final ||$secondsSinceCreate <= $rules->max_time_spent){
                        if($timestamp > $score->timestamp){
                            echo "Validation passed <br/>";
                            return true;
                        }else
                            echo "Bad timestamp : " . $timestamp . "<br/>";
                    }else
                        echo "max time exceeded " . $secondsSinceCreate;
                }else
                    echo "Increment Time Exceeded : " . $secondsSinceUpdate . ", Max time allowed: " . $rules->score_increment_time . "<br/>";
            }else
                echo "Max Score Exceeded<br/>";
        }else
            echo "Bad increment<br/>";
        return false;
    }

    public function showLeaderboards(){
        $allGames = new Game();
        $percentiles = new Percentile();
        $gameStats = new GameStat();

        $firstGames = $allGames->orderBy('created_at', 'desc')->take(5)->get();

        $topUsers = $percentiles->orderBy('overall_percentile', 'desc')->take(10)->get();
        $dedicatedUsers = $percentiles->orderBy('play_count', 'desc')->take(10)->get();
        $decoratedUsers = $percentiles->orderBy('trophy_count', 'desc')->take(10)->get();

        $bestGames = $gameStats->orderBy('rating', 'desc')->take(10)->get();
        $mostPlayedGames = $gameStats->orderBy('play_count', 'desc')->take(10)->get();
        $timeConsumingGames = $gameStats->orderBy('time_spent', 'desc')->take(10)->get();

        return view('leaderBoards', [
            'games' => $firstGames,
            'topUsers' => $topUsers,
            'dedicatedUsers' => $dedicatedUsers,
            'decoratedUsers' => $decoratedUsers,
            'bestGames' => $bestGames,
            'mostPlayedGames' => $mostPlayedGames,
            'timeConsumingGames' => $timeConsumingGames,
            'allGames' => $allGames->orderBy('created_at', 'desc')->get()
        ]);
    }
}
