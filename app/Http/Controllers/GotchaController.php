<?php

namespace App\Http\Controllers;

use App\GotchaPlayer;
use Illuminate\Http\Request;

class GotchaController extends Controller
{
    protected $adminPass = 'purse';

    public function create(Request $request){
        $name = $request->get('name');
        $dorm = $request->get('dorm');
        $isWaldock = $dorm === 'Waldock';
        $targetID = $request->get('target');
        $targetedByID = $request->get('targetedBy');
        $alive = 1;
        $moreInfo = $request->get('moreInfo') ?: '-';
        $moreInfo .= '</br/>';
        $password = $request->get('password');
        if(!GotchaPlayer::where('name', '=', $name)->first())
            if($password === $this->adminPass) {
                $newPlayer = GotchaPlayer::create([
                    'name' => $name,
                    'dorm' => $dorm,
                    'is_waldock' => $isWaldock,
                    'target' => $targetID,
                    'targeted_by' => $targetedByID,
                    'alive' => $alive,
                    'more_info' => $moreInfo
                ]);

                if ($targetedByID)
                    GotchaPlayer::where('id', '=', $targetedByID)->first()->update(['target' => $newPlayer->id]);

                if ($targetID)
                    GotchaPlayer::where('id', '=', $targetID)->first()->update(['targeted_by' => $newPlayer->id]);
            }
        return redirect('/waldock');
    }

    public function update(Request $request, $id){
        $playerID = $id;
        $player = GotchaPlayer::where('id', '=', $playerID)->first();
        $name = $request->get('name') ?: $player->name;
        $dorm = $request->get('dorm') ?: $player->dorm;
        $isWaldock = $dorm === 'Waldock';
        $targetID = $request->get('target') ?: $player->target ? $player->target->id : 0;
        $targetedByID = $request->get('targetedBy') ?: $player->targeted_by->id;
        $moreInfo = $request->get('moreInfo') ?: $player->more_info;
        $password = $request->get('password');

        if($password === $this->adminPass) {
            $player->update([
                'name' => $name,
                'dorm' => $dorm,
                'is_waldock' => $isWaldock,
                'target' => $targetID,
                'targeted_by' => $targetedByID,
                'more_info' => $moreInfo
            ]);

            if ($targetedByID)
                GotchaPlayer::where('id', '=', $targetedByID)->update(['target' => $player->id]);

            if ($targetID)
                GotchaPlayer::where('id', '=', $targetID)->update(['targeted_by' => $player->id]);
        }
        return redirect('/waldock');
    }

    public function choosePlayer(){
        $allPlayers = GotchaPlayer::where('alive', '=', 1)->orderBy('name', 'asc')->get();
        return view('gotcha.choosePlayer', [
            'allPlayers' => $allPlayers
        ]);
    }

    public function killPlayer(Request $request)
    {
        $killedID = $request->get('id');
        $password = $request->get('password');
        $killerID = $request->get('killerID');
        if ($password === $this->adminPass) {
            $killed = GotchaPlayer::find($killedID);
            $killer = GotchaPlayer::find($killerID);
            $killed->killedBy($killer);
            return "Correct password";
        }
        return "Failed password";
    }

    public function newPlayer(){
        $allPlayers = GotchaPlayer::where('alive', '=', 1)->orderBy('name', 'asc')->get();
        return view('gotcha.createPlayer', [
            'allPlayers' => $allPlayers
        ]);
    }

    public function selectPlayer(Request $request){
        $id = $request->get('player');
        $allPlayers = GotchaPlayer::where('alive', '=', 1)->orderBy('name', 'asc')->get();
        return view('gotcha.editPlayer', [
            'allPlayers' => $allPlayers,
            'player' => GotchaPlayer::find($id)
        ]);
    }

    public function index(){
        $waldockPlayers = GotchaPlayer::where('is_waldock', '=', 1)->where('alive', '=', 1)->orderBy('name', 'asc')->get();
        $chains = $this->chains();
        $deadPlayers = GotchaPlayer::where('alive', '=', 0)->orderBy('name', 'asc')->get();
        $waldockTargets = [];
        $waldockTargetedBy = [];
        foreach($waldockPlayers as $w){
            if($w->target && $w->target->alive)
                array_push($waldockTargets, $w->target);
            if($w->targeted_by && $w->targeted_by->alive)
                array_push($waldockTargetedBy, $w->targeted_by);
        }

        return view('gotcha.home', [
            'waldockPlayers' => $waldockPlayers,
            'waldockTargets' => $waldockTargets,
            'waldockTargetedBy' => $waldockTargetedBy,
            'allPlayers' => GotchaPlayer::where('alive', '=', 1)->orderBy('name', 'asc')->get(),
            'killedPlayers' => $deadPlayers,
            'chains' => $chains
        ]);
    }

    public function addComment(Request $request){
        $id = $request->get('id');
        $comment = $request->get('comment');
        $player = GotchaPlayer::find($id);
        $oldComment = $player->more_info;
        if($comment)
            $player->update([
                'more_info' => $oldComment . $comment . '<br/>'
            ]);
        return $player->more_info;
    }

    public function login(){
        return view('gotcha.login');
    }

    public function postLogin(Request $request){
        session(['password' => $request->get('password')]);
        return redirect('/waldock');
    }

    public function chains(){
        $waldockPlayers = GotchaPlayer::where('is_waldock', '=', 1)->where('alive', '=', 1)->orderBy('name', 'asc')->get();
        $chains = [];
        foreach($waldockPlayers as $w){
            $chain = $w->chain();
            if($chain && count($chain) > 0)
                array_push($chains, $chain);
        }
        return $chains;
    }
}
