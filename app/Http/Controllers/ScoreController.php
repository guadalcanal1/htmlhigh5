<?php

namespace App\Http\Controllers;

use App\CurrentScore;
use App\Game;
use App\GameSetting;
use App\GameStart;
use App\Image;
use App\IP;
use App\IpUser;
use App\Rating;
use App\RemotePlay;
use App\Score;
use App\TimeSpent;
use App\Trophy;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $slug)
    {
        var_dump($request->all());
        $gameID = Game::where('slug', '=', $slug)->first()->id;
        $ip = $request->get('ip') ?: $request->ip();
        if(!Game::find($gameID))
            return abort(404);
        $userID = null;
        if(Auth::user())
            $userID = Auth::user()->id;
        if(!$userID)
            $userID = ScoreController::getUserIDFromIP($ip);
        if(!$userID)
            $userID = -ScoreController::getSessionInt();
        echo "FOUND USER ID TO BE $userID";
        $extant = CurrentScore::where('game_id', '=', $gameID)->where(function($query) use($userID, $ip){
            $query->where('user_id', '=', $userID)
                ->orWhere('ip', '=', $ip);
        })->get();
        if ($extant)
            foreach ($extant as $score)
                $score->delete();
        $time = Carbon::now()->subSeconds(5);
        echo 'Got here';
        GameStart::create([
            'user_id' => $userID,
            'game_id' => $gameID
        ]);
        echo 'got here 2';
        CurrentScore::create([
            'game_id' => $gameID,
            'user_id' => $userID,
            'score' => 0,
            'timestamp' => 0,
            'updated_at' => $time,
            'created_at' => $time,
            'ip' => $ip
        ]);
        echo 'finished';
    }

    public function addTrophy(Request $request, $slug)
    {
        $tag = $request->get('tag');
        $gameID = Game::where('slug', '=', $slug)->first()->id;
        $userID = null;
        $user = Auth::user();
        $ip = $request->get('ip') ?: $request->ip();
        if(!$user) {
            $userID = ScoreController::getUserIDFromIP($ip);
            if($userID)
                $user = User::find($userID);
        }
        if(!$user || !Game::find($gameID))
            return abort(404);
        $trophy = Trophy::where('unlock_key', '=', $tag)->first();
        if(!$trophy->user()->where('user_id', '=', $user->id)->first())
            $trophy->user()->save($user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $slug)
    {
        var_dump($request->all());
        $gameID = Game::where('slug', '=', $slug)->first()->id;
        if(!Game::find($gameID))
            return abort(404);
        $increment = $request->get('increment');
        $hash = $request->get('hash');
        $timestamp = $request->get('timestamp');
        $score = null;
        $scoreValue = 0;
        $ip = $request->get('ip') ?: $request->ip();
        echo 'storing. IP: ' . $ip;
        $user = null;
        $userID = null;
        $user = Auth::user();
        if(!$user) {
            $userID = ScoreController::getUserIDFromIP($ip);
            if($userID)
                $user = User::find($userID);
        }
        if($user){
            $score = $user->currentScoreOn($gameID);
            $userID = $user->id;
        }else {
            $userID = -ScoreController::getSessionInt();
            $score = CurrentScore::where('user_id', '=', $userID)->first();
        }
        echo "FOUND USER ID TO BE $userID";
        if(!$score)
            $score = CurrentScore::where('ip', '=', $ip)->first();
        if ($this->hash($increment, $timestamp) === $hash  || $slug === 'popsicio') {
            echo 'The hash is equal or it is popsicio. Score: ' . $scoreValue;
            if ($score) {
                $scoreValue += $score->score;
                echo "Validating";
                if (GameController::validateScore($increment, $timestamp, $gameID, $scoreValue + $increment, $score, true)) {
                    echo " [[VALIDATION PASSED]]";
                    echo "FINAL SCORE: " . ($scoreValue + $increment);;
                    Score::create([
                        'game_id' => $gameID,
                        'user_id' => $userID,
                        'value' => $scoreValue + $increment
                    ]);
                    $secondsSinceCreate = $score->created_at->diffInSeconds(Carbon::now());
                    echo "TIME SPENT: " . $secondsSinceCreate;
                    $score->delete();
                    $timeSpent = TimeSpent::create([
                        'user_id' => $userID,
                        'game_id' => $gameID,
                        'seconds' => min($secondsSinceCreate, 1000)
                    ]);
                    if($user)
                        $user->timeSpent()->save($timeSpent);
                }else
                    echo "VALIDATION FAILED";
            }
        }else{
            echo 'The hash is not equal and it is not popsicio';
        }
        echo 'finished updating';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {

    }

    public function remotePlay(Request $request){
        $url = $request->get('url');
        $game = $request->get('game');
        RemotePlay::create([
            'game'=>$game,
            'url'=>$url
        ]);
        return 'Remote play registered!';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        var_dump($request->all());
        $gameID = Game::where('slug', '=', $slug)->first()->id;
        if(!Game::find($gameID))
            return abort(404);
        $score = null;
        $scoreValue = 0;
        $user = null;
        $userID = null;
        $user = Auth::user();
        $ip = $request->get('ip') ?: $request->ip();
        echo 'Updating. IP: ' . $ip;
        if(!$user) {
            $userID = ScoreController::getUserIDFromIP($ip);
            if($userID)
                $user = User::find($userID);
        }
        if(!$user)
            $userID = -ScoreController::getSessionInt();
        if($user){
            $score = $user->currentScoreOn($gameID);
            $userID = $user->id;
        }else{
            echo "USER ID: " . $userID . "<br/>";
            $score = CurrentScore::where('user_id', '=', $userID)->first();
        }
        echo "FOUND USER ID TO BE $userID";
        $increment = $request->get('score');
        $hash = $request->get('hash');
        $timestamp = $request->get('timestamp');
        if(!$score)
            $score = CurrentScore::where('ip', '=', $request->ip())->first();
        if($this->hash($increment, $timestamp) === $hash || $slug == 'popsicio'){
            echo "HASH SUCCESS<br/>";
            if($score) {
                echo "SCORE<br/>";
                $scoreValue += $score->score;
                if (GameController::validateScore($increment, $timestamp, $gameID, $scoreValue + $increment, $score, false)) {
                    echo "PASSED<br/>";
                $score->update([
                    'game_id' => $gameID,
                    'user_id' => $userID,
                    'score' => $scoreValue + $increment,
                    'timestamp' => $timestamp
                ]);
                }else{
                    echo "VALIDATION FAILED<br/>";
                }
            }
        }else{
            echo 'HASH FAILED <br/>';
        }
        echo "Finished updating";
    }

    public static function getUserIDFromIP($ip){
        echo 'We are getting user from IP: ' . $ip;
        $model = IP::where('address', '=', $ip)->orderBy('updated_at', 'desc')->first();
        echo "Model Returned: ";
        var_dump($model);
        if(!$model)
            return null;
        echo "USER ID FROM IP: $model->user_id";
        return $model->user_id;
    }

    private function hash($value, $salt){
        $value = (string)$value;
        $salt = (string)$salt;
        $val = md5($value . md5($salt . md5($value . md5($salt))));
        for($i = 0; $i < 37; $i++)
            $val = md5($salt . $val);
        return $val;
    }

    public static function getSessionInt(){
        $id = Session::getId();
        echo "Session ID: " . Session::getId();
        $ret = 0;
        $length = strlen( $id );
        for( $i = 0; $i <= $length; $i++ ) {
            $char = substr( $id, $i, 1 );
            $ret += ($i+1) * 11 * ord($char) + ($i+1) * 10 * ord($char) % ($i + 1);
        }
        return $ret;
    }

    public function play($slug){

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function rate(Request $request){

    }
}
