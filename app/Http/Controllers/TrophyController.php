<?php

namespace App\Http\Controllers;

use App\Game;
use App\Image;
use App\Trophy;
use Illuminate\Http\Request;

class TrophyController extends Controller
{
    public function store(Request $request){
        if(!AdminController::isAdmin()){
            return redirect('/');
        } else {
            $game_id = $request->get('game');
            $game = Game::find($game_id)->first();
            $sort_value = $request->get('sort_value');
            $description = $request->get('description');
            $unlock_key = $request->get('unlock_key');
            $name = $request->get('name');
            $image = $request->file('image');
            $trophyImage = Image::create([
                'alt_text' => $game->name . ' trophy'
            ]);
            $image->move(public_path() . '/images/gameImages/' . $trophyImage->id . '/', 'original.png');
            Trophy::create([
                'name' => $name,
                'description' => $description,
                'sort_value' => $sort_value,
                'unlock_key' => $unlock_key,
                'image' => $trophyImage->id,
                'game_id' => $game_id
            ]);
        }
    }
}
