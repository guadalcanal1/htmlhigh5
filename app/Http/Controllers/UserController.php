<?php

namespace App\Http\Controllers;

use App\Percentile;
use App\User;
use App\Game;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $user = User::where('slug', $slug)->first();
        $percentile = $user->percentileStats();
        $trophies = $user->trophies()->orderBy('game_id', 'asc')->orderBy('sort_value', 'asc')->get();
        $overallRating = $percentile ? Percentile::all()->where('overall_percentile', '>=', $percentile->overall_percentile)->count() : 0;
        $playCountRating = $percentile ? Percentile::all()->where('play_count', '>=', $percentile->play_count)->count() : 0;
        $trophyRating = $percentile ? Percentile::all()->where('trophy_count', '>=', $percentile->trophy_count)->count() : 0;
        $gamesPlayed = $user->gamesPlayed();
        $gamePercentiles = [];
        $gameNames = [];
        foreach($gamesPlayed as $game) {
            $gamePercentiles[] = 100 * $user->gamePercentile($game->game_id);
            $gameNames[] = Game::find($game->game_id)->name;
        }

        return view('user.show', [
            'user' => $user,
            'percentile' => $percentile,
            'trophies' => $trophies,
            'overallRating' => $overallRating,
            'playCountRating' => $playCountRating,
            'trophyRating' => $trophyRating,
            'gameNames' => $gameNames,
            'gamePercentiles' => $gamePercentiles
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function getUserID(){
        if(Auth::check())
            return Auth::user()->id;
        return -ScoreController::getSessionInt();
    }

    public function unsubscribe($token){
        $user = User::where('email_token', $token)->first();
        if($user && $user->is_subscribed == 1) {
            $user->is_subscribed = 0;
            $user->save();
        }
        return view('auth.unsubscribed');
    }
}
