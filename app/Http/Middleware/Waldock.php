<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class Waldock
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // Specifying a default value...
        $password = session('password', '');
        $day = Carbon::now()->day;
        $requiredPassword = '.';
        switch($day){
            case 11: $requiredPassword = 'FISH'; break;
            case 12: $requiredPassword = 'RIDE'; break;
            case 13: $requiredPassword = 'LOGS'; break;
            case 14: $requiredPassword = 'TOOL'; break;
            case 15: $requiredPassword = 'WINS'; break;
        }
        if($password !== $requiredPassword)
            return redirect('/waldock/login');
        // Store a piece of data in the session...

        return $next($request);
    }
}
