<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IP extends Model
{
    protected $table = 'ip';
    protected $fillable = ['address', 'user_id'];

    public function user(){
        return $this->hasMany('App\User');
    }
}
