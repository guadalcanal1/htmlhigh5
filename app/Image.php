<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManager;

class Image extends Model
{
    protected $fillable = ['alt_text'];

    public function game(){
        return $this->belongsTo('App\Game');
    }

    public function uploadImage(){
        $manager = new ImageManager(array('driver' => 'imagick'));
        $image = $manager->make('public/foo.jpg')->resize(300, 200);
    }

    public function getSrcAttribute(){
        return '/images/gameImages/' . $this->id . '/original.png';
    }
}
