<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IpUser extends Model
{
    protected $fillable = ['ip', 'user_id'];
}
