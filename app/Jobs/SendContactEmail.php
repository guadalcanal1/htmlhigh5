<?php

namespace App\Jobs;

use App\Mail\ContactMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendContactEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mailFrom, $mailMessage;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($from, $message)
    {
        $this->mailFrom = $from;
        $this->mailMessage = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new ContactMail($this->mailFrom, $this->mailMessage);
        $email->to("htmlhigh5@gmail.com", "CONTACT");
        $email->from("contact@htmlhigh5.com", 'Contact Form');
        Mail::to("htmlhigh5@gmail.com", "CONTACT")->send($email);
    }
}
