<?php

namespace App\Jobs;

use App\Mail\EmailNewLook;
use App\Mail\NewGameEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendNewGameEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $game;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $game)
    {
        $this->user = $user;
        $this->game = $game;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new NewGameEmail($this->user, $this->game);
        $email->to($this->user->email, $this->user->name);
        $email->subject($this->game->name . ': New Game!');
        $email->from('contact@htmlhigh5.com', 'HTMLHigh5');
        Mail::to($this->user->email, $this->user->name)->send($email);
    }
}
