<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewGameEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $game;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $game)
    {
        $this->user = $user;
        $this->game = $game;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.newGame')->with([
            'userToken' => $this->user->email_token,
            'name' => $this->game->name,
            'slug' => $this->game->slug,
            'image' => $this->game->cover_image_id
        ]);
    }
}
