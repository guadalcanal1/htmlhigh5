<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageLoad extends Model
{
    protected $table = 'page_load';

    protected $fillable = [
      'info', 'user_id'
    ];
}
