<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Percentile extends Model
{
    protected $table = 'user_percentile';

    protected $fillable = [
        'user_id', 'overall_percentile', 'partial_percentile',
        'games_played', 'play_count', 'time_spent', 'trophy_count'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
