<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayPageLoad extends Model
{
    protected $table = 'play_page_load';

    protected $fillable = [
      'info', 'user_id'
    ];
}
