<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemotePlay extends Model
{
    protected $fillable = ['game', 'url'];
}
