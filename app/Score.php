<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $fillable = ['user_id', 'game_id', 'value'];

    public function game(){
        return $this->belongsTo('App\Game');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
