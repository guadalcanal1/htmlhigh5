<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScoreDistribution extends Model
{
    protected $guarded = [];

    protected $casts = [
        'score_data' => 'object'
    ];

    public function game(){
        return $this->belongsTo('App\Game');
    }
}
