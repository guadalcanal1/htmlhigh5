<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeSpent extends Model
{
    protected $table = 'time_spent';

    protected $fillable = ['seconds', 'game_id', 'user_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function game(){
        return $this->belongsTo('App\Game');
    }
}
