<?php

namespace App;

use App\Http\Controllers\AdminController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Trophy extends Model
{
    protected $table = 'trophies';

    protected $fillable = [
        'game_id', 'description', 'unlock_key', 'name', 'sort_value', 'image'
    ];

    public function user(){
        return $this->belongsToMany('App\User');
    }

    public function game(){
        return $this->belongsTo('App\Game');
    }

    function getImageAttribute($id){
        return Image::find($id);
    }
}
