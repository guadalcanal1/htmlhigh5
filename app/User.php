<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'slug', 'email_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scores(){
        return $this->hasMany('App\Score');
    }

    public function playCount($gameId){
        $plays = $this->scores()->where('game_id', '=', $gameId)->get();
        if(!$plays)
            return 0;
        return $plays->count();
    }

    public function averageScore($gameId){
        $scores = $this->scores()->where('game_id', '=', $gameId)->get();
        if(!$scores)
            return 0;
        return $scores->avg('value');
    }

    public function highScore($gameId){
        $scores =  $this->scores()->where('game_id', '=', $gameId)->get();
        if(!$scores)
            return 0;
        return $scores->max('value');
    }

    public function gamesPlayed(){
        return $this->scores()->groupBy('game_id')->select('game_id')->get();
    }

    public function timeSpentOn($game){
        $times = $this->timeSpent()->where('game_id', '=', $game)->get();
        if(!$times)
            return '0:00:00:00';
        $seconds = $times->sum('seconds');
        $days = floor($seconds / 60 / 60 / 24);
        return $days . ':' . gmdate("H:i:s", $seconds);
    }

    public function secondsSpentOn($game){
        $times = $this->timeSpent()->where('game_id', '=', $game)->get();
        if(!$times)
            return 0;
        return $times->sum('seconds');
    }

    public function gamePercentile($gameId){
        $game = Game::find($gameId);
        $myScore = $this->highScore($gameId);
        if(!$myScore)
            return 0;
        $allHighScores = $game->allHighScores();
        $numHighScores = $allHighScores->get()->count();
        $worseHighScores = $allHighScores->get()->where('value', '<=', $myScore)->count();
        return $worseHighScores/$numHighScores;
    }

    public function hasUnlockedTrophy($id){
        return $this->trophies()->get()->find($id);
    }

    public function ratings(){
        return $this->hasMany('App\Rating');
    }

    public function currentScores(){
        return $this->hasMany('App\CurrentScore');
    }

    public function currentScoreOn($gameID){
        return $this->currentScores()->where('game_id', '=', $gameID)->first();
    }

    public function trophies(){
        return $this->belongsToMany('App\Trophy');
    }

    public function userPercentile(){
        return $this->hasOne('App\Percentile');
    }

    public function percentileStats(){
        return $this->userPercentile()->get()->first();
    }

    public function timeSpent(){
        return $this->hasMany('App\TimeSpent');
    }

    public function ips(){
        return $this->hasMany('App\IP');
    }

    public function currentIP(){
        $ip = $this->ips()->orderBy('updated_at', 'desc')->first();
        return $ip ? $ip->address : null;
    }

    public static function updateAllPercentiles(){
        $users = User::all();
        $games = Game::all();
        Percentile::truncate();
        foreach($users as $user){
            $percentile = 0;
            $gamesPlayed = 0;
            $totalPlaysCount = 0;
            $totalTimeSpent = 0;
            $trophyCount = $user->trophies()->get()->count();
            foreach($games as $game){
                $percentile += $user->gamePercentile($game->id);
                $totalPlaysCount += $user->playCount($game->id);
                $totalTimeSpent += $user->secondsSpentOn($game->id);
                if($percentile > 0)
                    $gamesPlayed++;
            }
            $overallPercentile = $percentile / $games->count();
            $partialPercentile = $gamesPlayed > 0 ? $percentile / $gamesPlayed : 0;
            Percentile::create([
                'user_id' => $user->id,
                'overall_percentile' => $overallPercentile,
                'partial_percentile' => $partialPercentile,
                'games_played' => $gamesPlayed,
                'play_count' => $totalPlaysCount,
                'time_spent' => $totalTimeSpent,
                'trophy_count' => $trophyCount
            ]);
        }
    }
}
