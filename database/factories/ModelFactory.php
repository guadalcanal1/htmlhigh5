<?php

use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $name = $faker->name;
    $slug = Str::slug($name);

    return [
        'name' => $name,
        'slug' => $slug,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('global'),
        'remember_token' => str_random(10),
        'is_admin' => true
    ];
});

$factory->define(App\Game::class, function (Faker\Generator $faker) {
    $name = $faker->name;
    $slug = Str::slug($name);


    return [
        'name' => $name,
        'genre_id' => $faker->numberBetween(0,4),
        'description' => $faker->paragraph(),
        'keywords' => 'These are the game keywords',
        'slug' => $slug,
        'cover_image_id' => 1,
        'thumbnail' => 1
    ];
});

$factory->define(App\Image::class, function (Faker\Generator $faker) {
    static $game;
    return [
        'alt_text' => $faker->sentence()
    ];
});

$factory->define(App\Rating::class, function (Faker\Generator $faker) {
    static $game;
    static $user;
    static $value;
    return [
        'game_id' => $game ?: $game = 1,
        'user_id' => $user ?: $user = 1,
        'value' => $value ?: $value = 1,
    ];
});

$factory->define(App\Score::class, function (Faker\Generator $faker) {
    static $game;
    static $user;
    static $value;
    return [
        'game_id' => $game ?: $game = 1,
        'user_id' => $user ?: $user = 1,
        'value' => $value ?: $value = $faker->numberBetween(0,1000)
    ];
});

$factory->define(App\TimeSpent::class, function (Faker\Generator $faker) {
    static $game;
    static $user;
    static $seconds;
    return [
        'game_id' => $game ?: $game = 1,
        'user_id' => $user ?: $user = 1,
        'seconds' => $seconds ?: $seconds = $faker->numberBetween(0,300)
    ];
});

$factory->define(App\Trophy::class, function (Faker\Generator $faker) {
    static $game;
    static $name;
    static $description;
    static $sortValue;
    static $unlockKey;
    static $image;
    return [
        'game_id' => $game ?: $game = 1,
        'sort_value' => $sortValue ?: $sortValue = 1,
        'unlock_key' => $unlockKey ?: $unlockKey = 1,
        'image' => $image ?: $image = 1,
        'name' => $name ?: $name = "Generic Trophy",
        'description' => $description ?: $description = $faker->sentence
    ];
});

$factory->define(App\GameSetting::class, function (Faker\Generator $faker) {
    static $game;
    static $maxScore;
    static $maxIncrement;
    static $incrementTime;
    static $maxTimeSpent;
    return [
        'game_id' => $game ?: $game = 1,
        'max_score' => $maxScore ?: $maxScore = 30000,
        'max_score_increment' => $maxIncrement ?: $maxIncrement = 100,
        'score_increment_time' => $incrementTime ?: $incrementTime = 10,
        'max_time_spent' => $maxTimeSpent ?: $maxTimeSpent = 300
    ];
});