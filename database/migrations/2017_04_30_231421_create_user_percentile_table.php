<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPercentileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_percentile', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->double('overall_percentile');
            $table->double('partial_percentile');
            $table->integer('games_played');
            $table->integer('play_count');
            $table->integer('time_spent');
            $table->integer('trophy_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_percentile');
    }
}
