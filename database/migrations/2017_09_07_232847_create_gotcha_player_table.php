<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGotchaPlayerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gotcha_players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('dorm');
            $table->text('more_info');
            $table->boolean('is_waldock');
            $table->boolean('alive');
            $table->integer('target')->nullable();
            $table->integer('targeted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gotcha_players');
    }
}
