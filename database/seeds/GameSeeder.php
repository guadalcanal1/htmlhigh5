<?php

use Illuminate\Database\Seeder;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Game::class, 5)->create()->each(function($game) {
            for ($n = 1; $n < 11; $n++) {
                for($i = 0; $i < 50; $i++) {
                    factory(App\Score::class, 1)->create([
                        'game_id' => $game->id,
                        'user_id' => $n,
                        'value' => random_int(1, 5000)
                    ]);
                    factory(App\TimeSpent::class, 1)->create([
                        'game_id' => $game->id,
                        'user_id' => $n,
                        'seconds' => random_int(1, 300)
                    ]);
                }
                factory(App\Rating::class, 1)->create([
                    'game_id' => $game->id,
                    'user_id' => $n,
                    'value' => random_int(1, 5)
                ]);
            }
            factory(App\GameSetting::class, 1)->create([
                'game_id' => $game->id,
                'max_score' => random_int(10000, 20000),
                'max_score_increment' => random_int(100, 300),
                'score_increment_time' => random_int(9,15),
                'max_time_spent' => random_int(180, 300)
            ]);
            factory(App\Trophy::class, 1)->create([
                'game_id' => $game->id,
                'sort_value' => 1,
                'unlock_key' => $game->id . random_int(100, 300),
                'image' => 2,
                'name' => 'Bronze Trophy'
            ]);
            factory(App\Trophy::class, 1)->create([
                'game_id' => $game->id,
                'sort_value' => 5,
                'unlock_key' => $game->id . random_int(100, 300),
                'image' => 3,
                'name' => 'Silver Trophy'
            ]);
            factory(App\Trophy::class, 1)->create([
                'game_id' => $game->id,
                'sort_value' => 10,
                'unlock_key' => $game->id . random_int(100, 300),
                'image' => 4,
                'name' => 'Gold Trophy'
            ]);
        });
    }
}
