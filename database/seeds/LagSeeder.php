<?php

use Illuminate\Database\Seeder;

class LagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $game = App\Game::first();
        for ($n = 1; $n < 11; $n++) {
            for($i = 0; $i < 2000; $i++) {
                factory(App\Score::class, 1)->create([
                    'game_id' => $game->id,
                    'user_id' => $n,
                    'value' => random_int(1, 5000)
                ]);
            }
        }
    }
}
