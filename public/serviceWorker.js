// //make sure that Service Workers are supported.
// if (navigator.serviceWorker) {
//     navigator.serviceWorker.register('/serviceWorker.js', {scope: '/'})
//         .then(function (registration) {
//             console.log(registration);
//         })
//         .catch(function (e) {
//             console.error(e);
//         })
// } else {
//     console.log('Service Worker is not supported in this browser.');
// }
//
// const offlineStuff = [
//     '/css/toCache/app.css',
//     '/js/toCache/app.js'
// ];
//
// self.addEventListener('install', function(event) {
//     console.log('Installed');
//     event.waitUntil(
//         caches.open('v1').then(function(cache) {
//             console.log('Cache opened');
//             return cache.addAll(offlineStuff);
//         })
//     );
// });
//
// self.addEventListener('activate', function (event) {
//     event.waitUntil(
//         caches
//             .keys()
//             .then((keys) => {
//                 return Promise.all(
//                     keys
//                         .filter((key) => {
//                             //If your cache name don't start with the current version...
//                             return !key.startsWith(version);
//                         })
//                         .map((key) => {
//                             //...YOU WILL BE DELETED
//                             return caches.delete(key);
//                         })
//                 );
//             })
//             .then(() => {
//                 console.log('WORKER:: activation completed. This is not even my final form');
//             })
//     )
// });
//
// self.addEventListener('fetch', function(event) {
//     event.respondWith(
//         // Try the cache
//         caches.match(event.request).then(function(response) {
//             return response || fetch(event.request);
//         }).catch(function() {
//             //Error stuff
//         })
//     );
// });