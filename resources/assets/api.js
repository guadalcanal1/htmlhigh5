/*                         */
/***                     ***/
/*****                 *****/
/*******HTMLHigh5 API*******/
/*****                 *****/ 
/***                     ***/
/*                         */

//For registering a play. This should be run once and ONLY once, when the game is loaded.
//The $ is from JQuery, you will need to import it.

var url = (window.location !== window.parent.location)
        ? document.referrer
        : document.location.href;
$.post('https://htmlhigh5.com/remotePlay', {url: url, game: 'GAME NAME HERE'});


//For starting a game. This should be called when a new game has begun. YOUR_GAME_SLUG_HERE is the slug of your game name. (Popsic.io -> popsicio, Punt Hooligan -> punt-hooligan)
//Look up what a slug is if you are not familiar with it

$.post('https://htmlhigh5.com/play/$YOUR_GAME_SLUG_HERE/score/create');


//This is the complicated part for updating the score. The way the scoring API works is as follows: a score is created, then it is updated repeatedly, then it is stored.
//This was created to make cheating the score system more difficult
//The hash function uses MD5 hashing multiple times to make it a little more confusing. You may need to find a javascript library for md5 generation.
//NOTE: ONLY THE INCREMENT SINCE THE LAST SUBMISSION IS POSTED. NOT THE TOTAL SCORE
//The timestamp must be greater than the previous submitted timestamp

var increment = currentScore - lastScore;
lastScore = currentScore;
timeStamp = timeStamp + Math.ceil(Math.random() * 5);

$.post('https://htmlhigh5.com/play/$YOUR_GAME_SLUG_HERE/score/update',{timestamp: timeStamp, score: increment, hash: hashScore(increment, timeStamp)});

function hashScore(value, salt){
	var hashedValue = md5(value + md5(salt + md5(value + md5(salt))));
	for(var i = 0; i < 37; i++)
		hashedValue = md5(salt + hashedValue);
	return hashedValue;
}

//The store function is similar to the update function in most regards.

var increment = currentScore - lastScore;
lastScore = currentScore;
timeStamp = timeStamp + Math.ceil(Math.random() * 5);
$.post('https://htmlhigh5.com/play/$YOUR_GAME_SLUG_HERE/score/store',{timestamp: timestamp, increment: increment, hash: hashScore(increment, timestamp)});


//This will award a trophy provided a user is signed in. When a trophy is created, you can enter an unlock code. Enter this code here to specify the trophy

$.post('https://htmlhigh5.com/play/$YOUR_GAME_SLUG_HERE/trophy/add',{tag: 'UNLOCK_CODE'});