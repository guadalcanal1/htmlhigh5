
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

global.$ = global.jQuery = require('jquery');
require('./src/bootstrap.min');
//require('./src/jquery.isotope.min');
//require('./src/main');
//require('./src/homePage');
require('chart.js');
require('./src/admin.js');
require('./social/jssocials.min.js');
// require('../../../public/serviceWorker.js');