@extends('layouts.app')
@section('title')About Us @endsection
@section('content')
    <section id="aboutUsContent">
        <h1>About Us</h1>
        <p>
            HTML High 5 is a free-to-use website for enjoyable, high quality browser games. All games are
            mobile-compatible and free, and if you make an account
            you can save your high scores, view your statistics, and unlock trophies. Enjoy!</p>
        <h2>Free</h2>
        <p>
            You'll never have to pay a dime to enjoy all of our games to the fullest extent! All we have created is yours
            for the playing.
        </p>
        <h2>Fun</h2>
        <p>
            Above all, we design our games to create an enjoyable atmosphere for anyone playing them.
        </p>
        <h2>Mobile-Friendly</h2>
        <p>
            In this age of increasing mobile device usage, it is paramount for us to make all our games accessible to
            mobile users. Whether you're visiting us on your iPhone, Android, or Google Phone, our games are available
            and here to entertain!
        </p>
        <h2>High Quality</h2>
        <p>
            We have a high bar for entry onto our website. Trashy or otherwise low-quality games will be denied access
            so you don't need to be cautious.
        </p>
    </section>
@endsection