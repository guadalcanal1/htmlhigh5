@extends('layouts.admin')

@section('title') Create a New Game @endsection
@php
    //Might not be necessary, but can't hurt.
    if(!Auth::user() || !Auth::user()->is_admin){
        return redirect('/');
    }
@endphp
@section('content')
    <div class="adminFormContainer">
        <h1>Create a Game</h1>
        <form action="/game/store" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <label>Name</label><br />
            <input title="Name" type="text" name="name"><br />

            <label>Description</label><br />
            <textarea title="Description" name="description"></textarea><br />

            <label>Genre</label><br />
            <select title="Genre" name="genre">
                <option selected disabled>Choose One</option>
                @foreach(\App\GameGenre::all() as $genre)
                    <option value="{{$genre->id}}">{{$genre->name}}</option>
                @endforeach
            </select><br />

            <label>Keywords</label><br />
            <input title="Keywords" type="text" name="keywords"><br />

            <label>Cover Image (600x400)</label><br />
            <input title="Cover Image" type="file" name="cover_image" accept="image/*"><br />

            <label>Thumbnail Image (100x100)</label><br />
            <input title="Thumbnail Image" type="file" name="thumbnail_image" accept="image/*"><br />

            <label>Game Files Zipped (Must have index.html in root)</label><br />
            <input title="Zip" type="file" name="zip"><br />

            <h2>Game Settings</h2>

            <label>Max Possible Score</label><br />
            <input title="Max Score" type="number" name="max_score"><br />

            <label>Max Possible Score Increment</label><br />
            <input title="Max Score Increment" type="number" name="max_score_increment"><br />

            <label>Score Increment Time</label><br />
            <input title="Score Increment Time" type="number" name="score_increment_time"><br />

            <label>Max Time Spent</label><br />
            <input title="Max Time Spent" type="number" name="max_time_spent"><br />

            <input type="submit" value="Submit">
        </form>
    </div>
@endsection