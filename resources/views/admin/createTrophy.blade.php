@extends('layouts.admin')

@section('title') Create a New Trophy @endsection
@php
    //Might not be necessary, but can't hurt.
    if(!Auth::user() || !Auth::user()->is_admin){
        return redirect('/');
    }
@endphp
@section('content')
    <div class="adminFormContainer">
        <h1>Create a Trophy</h1>
        <form action="/trophy/store" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <label>Name</label><br />
            <input title="Name" type="text" name="name"><br />

            <label>Description</label><br />
            <textarea title="Description" name="description"></textarea><br />

            <label>Game</label><br />
            <select title="Game" name="game">
                <option selected disabled>Choose One</option>
                @foreach(\App\Game::all() as $game)
                    <option value="{{$game->id}}">{{$game->name}}</option>
                @endforeach
            </select><br />

            <label>Sort Value</label><br />
            <input title="Sort Value" type="text" name="sort_value"><br />

            <label>Image (200x200)</label><br />
            <input title="Image" type="file" name="image" accept="image/*"><br />

            <label>Unlock Key</label><br />
            <input title="Unlock Key" type="text" name="unlock_key"><br />

            <input type="submit" value="Submit">
        </form>
    </div>
@endsection