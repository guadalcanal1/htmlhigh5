@extends('layouts.admin')

@section('title') Admin Control Panel @endsection
@php
    //Might not be necessary, but can't hurt.
    if(!Auth::user() || !Auth::user()->is_admin){
        return redirect('/');
    }
@endphp
@section('content')

    <section id="adminHome">
        <h1>Welcome, {{Auth::user()->name}}</h1>

        <div id="adminButtons" class="col-sm-12">
            <a href="admin/createGame">
                <div class="buttonContainer col-sm-3">
                    <div class="adminButton">
                        <h2>Create Game</h2>
                    </div>
                </div>
            </a>
            <a href="admin/updateGame">
                <div class="buttonContainer col-sm-3">
                    <div class="adminButton">
                        <h2>Modify Game</h2>
                    </div>
                </div>
            </a>
            <a href="admin/createTrophy">
                <div class="buttonContainer col-sm-3">
                    <div class="adminButton">
                        <h2>Create Trophy</h2>
                    </div>
                </div>
            </a>
            <a href="admin/updateTrophy">
                <div class="buttonContainer col-sm-3">
                    <div class="adminButton">
                        <h2>Modify Trophy</h2>
                    </div>
                </div>
            </a>
            <a href="admin/stats/7/day">
                <div class="buttonContainer col-sm-3">
                    <div class="adminButton">
                        <h2>View Metrics</h2>
                    </div>
                </div>
            </a>
            <a href="admin/removeUser">
                <div class="buttonContainer col-sm-3">
                    <div class="adminButton">
                        <h2>Remove User</h2>
                    </div>
                </div>
            </a>
            <a href="admin/notifyUsers">
                <div class="buttonContainer col-sm-3">
                    <div class="adminButton">
                        <h2>Notify Users</h2>
                    </div>
                </div>
            </a>
        </div>
    </section>

@endsection