@extends('layouts.admin')

@section('title') Modify a Game @endsection
@php
    //Might not be necessary, but can't hurt.
    if(!Auth::user() || !Auth::user()->is_admin){
        return redirect('/');
    }
@endphp
@section('content')
    <div class="adminFormContainer">
        <h1>Select a Game to Modify</h1>
        <form action="#" method="get">

            <label>Game</label><br />
            <select id='game' title="Game" name="game">
                <option selected disabled>Choose One</option>
                @foreach(\App\Game::all() as $game)
                    <option value="{{$game->slug}}">{{$game->name}}</option>
                @endforeach
            </select>
        </form>
    </div>
    <script>
        $( "#game" ).change(function() {
            window.location = '/game/edit/' + $(this).val();
        });
    </script>
@endsection