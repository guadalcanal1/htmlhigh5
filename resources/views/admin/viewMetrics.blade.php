@extends('layouts.admin')

@section('title') Metrics @endsection
@php
    //Might not be necessary, but can't hurt.
    if(!Auth::user() || !Auth::user()->is_admin){
        abort(404);
    }
@endphp
@section('content')
<div id="adminStats">
    <form action="/admin/switch" method="post">
        <p>Time Increment</p>
        <label>Minute</label><input title="Minute" type="radio" name="unit" value="minute"><br/>
        <label>Hour</label><input title="Hour" type="radio" name="unit" value="hour"><br/>
        <label>Day</label><input title="Day" type="radio" name="unit" value="day" checked><br/>
        <label>Week</label><input title="Week" type="radio" name="unit" value="week"><br/>
        <label>Month</label><input title="Month" type="radio" name="unit" value="month"><br/>
        <label>Amount</label><input title="Amount" type="number" name="count" value="7"><br/>
        <input type="submit" value="Submit"><br/>
    </form>
    <section id="weeklyPlays" class="graphSection">
        <h2>Weekly Plays</h2>
        <div class="col-md-10 col-md-offset-1 graphContainer">
            <canvas id="playsThisWeek"></canvas>
        </div>
    </section>
    <section id="newAccounts" class="graphSection">
        <h2>New Accounts this Week</h2>
        <div class="col-md-10 col-md-offset-1 graphContainer">
            <canvas id="signupsThisWeek"></canvas>
        </div>
    </section>
    <section id="loads" class="graphSection">
        <h2>Page Loads</h2>
        <div class="col-md-10 col-md-offset-1 graphContainer">
            <canvas id="pageLoadsThisWeek"></canvas>
        </div>
    </section>
    <section id="clicks" class="graphSection">
        <h2>Play Button Clicks</h2>
        <div class="col-md-10 col-md-offset-1 graphContainer">
            <canvas id="buttonClicksThisWeek"></canvas>
        </div>
    </section>
    <section id="plays" class="graphSection">
        <h2>Game Loads this Week</h2>
        <div class="col-md-10 col-md-offset-1 graphContainer">
            <canvas id="playLoadsThisWeek"></canvas>
        </div>
    </section>
    <section id="plays" class="graphSection">
        <h2>Plays Today</h2>
        <div class="col-md-10 col-md-offset-1 graphContainer">
            <canvas id="dailyPlays"></canvas>
        </div>
    </section>
    <section id="tables">
        <div class="table col-md-6">
            <h3>Recently Viewed Pages</h3>
            @foreach($recentPages as $page)
                <p>{{$page}}</p>
            @endforeach
        </div>
        <div class="table col-md-6">
            <h3>Recently Played Games</h3>
            @foreach($recentPlays as $play)
                <p>{{$play}}</p>
            @endforeach
        </div>
    </section>

</div>

    <script>
         function plays(){
             //Plays this week
             const scores = {!! json_encode($scores) !!};
             const userScores = {!! json_encode($userScores) !!};
             const gamesStarted = {!! json_encode($gameStarts) !!}
             const increment = 1;
             let values = [];
             let labels = [];
             let userValues = [];
             for(let i = 0; i < scores.length; i++){
                 values[i] = scores[i];
                 userValues[i] = userScores[i];
                 labels[i] = (scores.length - 1 - i * Math.ceil(increment)) + ' days ago';
             }

             let weeklyPlaysCanvas = $("#playsThisWeek");
             let scoreChart = new Chart(weeklyPlaysCanvas, {
                 type: 'line',
                 data: {
                     labels: labels,
                     datasets: [{
                         label: 'Games Started',
                         data: gamesStarted,
                         backgroundColor: 'rgba(0, 84, 255, 0.25)',
                         borderColor: 'rgba(0, 84, 255, 1)',
                         borderWidth: 3
                     }, {
                         label: 'Num Plays',
                         data: values,
                         backgroundColor: 'rgba(211, 84, 0, 0.15)',
                         borderColor: 'rgba(211, 84, 0, 1)',
                         borderWidth: 3
                     }, {

                         label: 'Num Plays (While logged in)',
                         data: userValues,
                         backgroundColor: 'rgba(84, 255, 0, 0.25)',
                         borderColor: 'rgba(84, 211, 0, 1)',
                         borderWidth: 3
                     }]
                 },
                 options: {
                     legend: {labels:{fontColor:"white", fontSize: 18}},
                     scales: {
                         yAxes: [{
                             ticks: {
                                 fontColor: 'white',
                                 beginAtZero:true
                             }
                         }],
                         xAxes: [{
                             ticks: {
                                 fontColor: 'white'
                             }
                         }]
                     },
                     responsive: true,
                     scaleFontColor: "#FFFFFF",
                     pointLabelFontColor: "#FFFFFF",
                     maintainAspectRatio: false
                 }
             });
        }

         function pageLoads(){
             //Plays this week
             const userLoads = {!! json_encode($userLoads) !!};
             const scores = {!! json_encode($pageLoads) !!};

             const increment = 1;
             let values = [];
             let labels = [];
             for(let i = 0; i < scores.length; i++){
                 values[i] = scores[i];
                 labels[i] = (scores.length -1 - i * Math.ceil(increment)) + ' days ago';
             }

             let weeklyPlaysCanvas = $("#pageLoadsThisWeek");
             let scoreChart = new Chart(weeklyPlaysCanvas, {
                 type: 'line',
                 data: {
                     labels: labels,
                     datasets: [{
                         label: 'Num Page Loads',
                         data: values,
                         backgroundColor: 'rgba(211, 84, 0, 0.15)',
                         borderColor: 'rgba(211, 84, 0, 1)',
                         borderWidth: 3
                     },
                     {

                         label: 'Num Page Loads (While logged in)',
                         data: userLoads,
                         backgroundColor: 'rgba(84, 255, 0, 0.25)',
                         borderColor: 'rgba(84, 211, 0, 1)',
                         borderWidth: 3
                     }]
                 },
                 options: {
                     legend: {labels:{fontColor:"white", fontSize: 18}},
                     scales: {
                         yAxes: [{
                             ticks: {
                                 fontColor: 'white',
                                 beginAtZero:true
                             }
                         }],
                         xAxes: [{
                             ticks: {
                                 fontColor: 'white'
                             }
                         }]
                     },
                     responsive: true,
                     scaleFontColor: "#FFFFFF",
                     pointLabelFontColor: "#FFFFFF",
                     maintainAspectRatio: false
                 }
             });
         }

         function buttonClicks(){
             //Plays this week
             const scores = {!! json_encode($buttonClicks) !!};
             const increment = 1;
             let values = [];
             let labels = [];
             for(let i = 0; i < scores.length; i++){
                 values[i] = scores[i];
                 labels[i] = (scores.length - 1 - i * Math.ceil(increment)) + ' days ago';
             }

             let weeklyPlaysCanvas = $("#buttonClicksThisWeek");
             let scoreChart = new Chart(weeklyPlaysCanvas, {
                 type: 'line',
                 data: {
                     labels: labels,
                     datasets: [{
                         label: 'Num Play Button Clicks',
                         data: values,
                         backgroundColor: 'rgba(211, 84, 0, 0.15)',
                         borderColor: 'rgba(211, 84, 0, 1)',
                         borderWidth: 3
                     }]
                 },
                 options: {
                     legend: {labels:{fontColor:"white", fontSize: 18}},
                     scales: {
                         yAxes: [{
                             ticks: {
                                 fontColor: 'white',
                                 beginAtZero:true
                             }
                         }],
                         xAxes: [{
                             ticks: {
                                 fontColor: 'white'
                             }
                         }]
                     },
                     responsive: true,
                     scaleFontColor: "#FFFFFF",
                     pointLabelFontColor: "#FFFFFF",
                     maintainAspectRatio: false
                 }
             });
         }

         function playLoads(){
             //Plays this week
             const scores = {!! json_encode($playLoads) !!};
             const increment = 1;
             let values = [];
             let labels = [];
             for(let i = 0; i < scores.length; i++){
                 values[i] = scores[i];
                 labels[i] = (scores.length - 1 - i * Math.ceil(increment)) + ' days ago';
             }

             let weeklyPlaysCanvas = $("#playLoadsThisWeek");
             let scoreChart = new Chart(weeklyPlaysCanvas, {
                 type: 'line',
                 data: {
                     labels: labels,
                     datasets: [{
                         label: 'Play Page Load Count',
                         data: values,
                         backgroundColor: 'rgba(211, 84, 0, 0.15)',
                         borderColor: 'rgba(211, 84, 0, 1)',
                         borderWidth: 3
                     }]
                 },
                 options: {
                     legend: {labels:{fontColor:"white", fontSize: 18}},
                     scales: {
                         yAxes: [{
                             ticks: {
                                 fontColor: 'white',
                                 beginAtZero:true
                             }
                         }],
                         xAxes: [{
                             ticks: {
                                 fontColor: 'white'
                             }
                         }]
                     },
                     responsive: true,
                     scaleFontColor: "#FFFFFF",
                     pointLabelFontColor: "#FFFFFF",
                     maintainAspectRatio: false
                 }
             });
         }

        function signUps(){
            //SignUps this week
            const scores = {!! json_encode($signUps) !!};
            const increment = 1;
            let values = [];
            let labels = [];
            for(let i = 0; i < scores.length; i++){
                values[i] = scores[i];
                labels[i] = (scores.length - 1 - i * Math.ceil(increment)) + ' days ago';
            }

            let canvas = $("#signupsThisWeek");
            let chart = new Chart(canvas, {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [{
                        label: 'New Users',
                        data: values,
                        backgroundColor: 'rgba(211, 84, 0, 0.15)',
                        borderColor: 'rgba(211, 84, 0, 1)',
                        borderWidth: 3
                    }]
                },
                options: {
                    legend: {labels:{fontColor:"white", fontSize: 18}},
                    scales: {
                        yAxes: [{
                            ticks: {
                                fontColor: 'white',
                                beginAtZero:true
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                fontColor: 'white'
                            }
                        }]
                    },
                    responsive: true,
                    scaleFontColor: "#FFFFFF",
                    pointLabelFontColor: "#FFFFFF",
                    maintainAspectRatio: false
                }
            });
        }

         function dailyPlayDistribution(){
             //SignUps this week
             const scores = {!! $dailyPlays !!};
             const increment = 1;
             let values = [];
             let labels = [];
             for(let i = 0; i < scores.length; i++){
                 values[i] = scores[i].count;
                 labels[i] = scores[i].game_id;
             }

             let canvas = $("#dailyPlays");
             let chart = new Chart(canvas, {
                 type: 'doughnut',
                 data: {
                     labels: labels,
                     datasets: [{
                         label: 'Games Played Today',
                         data: values,
                         backgroundColor: ['#990000', '#009900', '#000099', '#999900', '#009999', '#990099', '#ff0000', '#00ff00', '#0000ff', '#ff0', '#f0f', '#00f'],
                         borderColor: '#fff',
                         borderWidth: 3
                     }]
                 },
                 options: {
                     legend: {labels:{fontColor:"white", fontSize: 18}},
                     scales: {
                         yAxes: [{
                             ticks: {
                                 fontColor: 'white',
                                 beginAtZero:true
                             }
                         }],
                         xAxes: [{
                             ticks: {
                                 fontColor: 'white'
                             }
                         }]
                     },
                     responsive: true,
                     scaleFontColor: "#FFFFFF",
                     pointLabelFontColor: "#FFFFFF",
                     maintainAspectRatio: false
                 }
             });
         }

        signUps();
        plays();
        playLoads();
        buttonClicks();
        pageLoads();
        dailyPlayDistribution();

    </script>
@endsection