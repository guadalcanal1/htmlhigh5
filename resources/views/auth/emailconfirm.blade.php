/**
 * Created by Ian on 8/2/2017.
 */
@extends('layouts.app')
@section('title') Email Confirmed! @endsection
@section('content')
<div id="container" class="col-md-12" style="text-align: center; margin-top: 50px; background-color:#121212; padding: 200px 0;">
 <p style="font-size: 28px;"><span style="color: #00b33d; font-weight: bold" >Email verified!</span> You are all set! Time to <a href="/">play some games!</a></p>
 <p>You will be automatically redirected in 3 seconds</p>
 <script>
   setTimeout(function(){
       window.location = "/"
   }, 3000);
 </script>
</div>
@endsection