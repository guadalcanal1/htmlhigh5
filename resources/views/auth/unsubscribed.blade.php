/**
 * Created by Ian on 8/2/2017.
 */
@extends('layouts.app')
@section('title') Unsubscribed! @endsection
@section('content')
<div id="container" class="col-md-12" style="text-align: center; margin-top: 50px; background-color:#121212; padding: 200px 0;">
 <p style="font-size: 28px;"><span style="color: #006494; font-weight: bold" >You have been unsubscribed!</span> We're sorry to see you go!
  You can still stick around and <a href="/">play some games!</a></p>
 <p>You will be automatically redirected in 3 seconds</p>
 <script>
   setTimeout(function(){
       window.location = "/"
   }, 3000);
 </script>
</div>
@endsection