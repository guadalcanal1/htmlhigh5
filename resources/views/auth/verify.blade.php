/**
 * Created by Ian on 8/2/2017.
 */
@extends('layouts.app')
@section('title') Email Sent! @endsection
@section('content')
<div id="container" class="col-md-12" style="text-align: center; margin-top: 50px; background-color:white; padding: 200px 0;">
    <p style="font-size: 28px;">An email was sent to <span style="font-weight: bold; color: #00b33d;">{{$email}}</span>. Click the button inside to verify your account!</p>
    {{--<a style="font-size: 22px" href="/resendEmail/{{$email}}">Click here to resend</a>--}}
</div>
@endsection