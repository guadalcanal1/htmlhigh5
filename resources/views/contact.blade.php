@extends('layouts.app')
@section('title') Contact Us @endsection
@section('content')
    <div id="contactContainer" class="col-md-12">
        {{Form::open(['url' => '/sendContact'])}}
            {{Form::label('email', 'EMail Address', ['class' => 'contactLabel'])}}
            <br/>
            {{Form::text('email', null, ['class' => 'contactInput', 'required' => 'required'])}}
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
            <br/>
            {{Form::label('message', 'Message', ['class' => 'contactLabel'])}}
            <br/>
            {{Form::textarea('message', null, ['class' => 'contactInput', 'required' => 'required'])}}
            @if ($errors->has('message'))
                <span class="help-block">
                    <strong>{{ $errors->first('message') }}</strong>
                </span>
            @endif
            <br/>
            <div class="recaptchaContainer">
                {!! Recaptcha::render() !!}
            </div>
            @if ($errors->has('recaptcha_response_field'))
                <span class="help-block">
                        <strong>{{ $errors->first('recaptcha_response_field') }}</strong>
                    </span>
            @endif
          <br/>
            {{Form::submit('Submit')}}
        {{Form::close()}}

        <p id="footerText">You can also send us an email at <span class="email">contact@htmlhigh5.com</span></p>
    </div>
@endsection