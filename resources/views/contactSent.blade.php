/**
* Created by Ian on 8/3/2017.
*/
@extends('layouts.app')
@section('title') Contact Sent @endsection
@section('content')
    <div id="container" class="col-md-12" style="text-align: center; margin-top: 50px; background-color:white; padding: 200px 0;">
        <p style="font-size: 28px;"><span style="color: #00b33d; font-weight: bold" >Message Sent!</span> We will get back to you as soon as possible! In the meantime, <a href="/">play some games!</a></p>
        <p>You will be automatically redirected in 5 seconds</p>
        <script>
            setTimeout(function(){
                window.location = "/"
            }, 5000);
        </script>
    </div>
@endsection