<html>
<head></head>
<body style="text-align: center; background-color: #121212;">
    <div style="text-align: center">
        <h1 style="font-size: 50px; color: #fff; margin-top: 0; padding: 20px; background-color: #006494">Ready for a new game?</h1>
        <p style="font-size: 22px"><strong>{{$name}}</strong> is now available on HTML High 5! Since you are one of our <em>most valued members</em>, we thought you might like to be one of the first to try it out!</p>
        <p style="font-size: 22px">We hope you enjoy the experience; don't forget to challenge your friends!</p>
        <a style="font-size: 24px; margin: 15px; padding: 15px; border-radius: 5px; background-color: #006494; color: white; display: inline-block; text-decoration: none;" href="https://htmlhigh5.com/game/{{$slug}}" target="_blank">Play {{$name}} Now!</a><br/>
        <img style="max-width: 100%; margin: 0 auto;" alt="{{$name}}" src="https://htmlhigh5.com/images/gameImages/{{$image->id}}/original.png">
        <p style="font-size: 18px; margin-top: 10px;">If you are having trouble clicking the link, you can copy and paste this text into your browser: https://htmlhigh5.com/game/{{$slug}}</p>
        <p style="margin-top: 50px;">&copy;{{\Carbon\Carbon::now()->year}} <a target="_blank" href="https://htmlhigh5.com/" title="Free HTML5 Games">HTML High 5</a>. All Rights Reserved.</p>
        <div style="background-color: #006494; padding: 20px; text-align: center">
            <img style="max-width: 100%" src="https://htmlhigh5.com/images/logo_menu_medium.png" alt="Logo">
        </div>
        <p style="font-size: 12px">
            <a href="https://htmlhigh5.com/{{$userToken}}/unsubscribe" target="_blank">Unsubscribe</a>
        </p>
    </div>
</body>
</html>