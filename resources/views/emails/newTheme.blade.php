<html>
<head></head>
<body style="text-align: center; background-color: #121212; color: #ffffff;">
    <div style="text-align: center">
        <h1 style="font-size: 50px; color: #ffffff; margin-top: 0; padding: 20px; background-color: #006494">HTML High 5 has a new look!</h1>
        <p style="font-size: 22px">There is a lot more coming your way this summer!</p>
        <p style="font-size: 22px">Don't worry, we won't flood your inbox. We are making exciting new content and we're excited to share it with you!</p>
        <a style="font-size: 24px; margin: 15px; padding: 15px; border-radius: 5px; background-color: #006494; color: white; display: inline-block; text-decoration: none;" href="https://htmlhigh5.com/" target="_blank">See for Yourself!</a>
        <p style="font-size: 18px; margin-top: 10px;">We have new games coming up in the very near future, stay tuned!</p>
        <p style="margin-top: 50px;">&copy;{{\Carbon\Carbon::now()->year}} <a target="_blank" href="https://htmlhigh5.com/" title="Free HTML5 Games">HTML High 5</a>. All Rights Reserved.</p>
        <div style="background-color: #006494; padding: 20px; text-align: center">
            <img style="max-width: 100%" src="https://htmlhigh5.com/images/logo_menu_medium.png" alt="Logo">
        </div>
        <p style="font-size: 12px">
            <a href="https://htmlhigh5.com/{{$userToken}}/unsubscribe" target="_blank">Unsubscribe</a>
        </p>
    </div>
</body>
</html>