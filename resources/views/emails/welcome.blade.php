<html>
<head></head>
<body style="text-align: center; background-color: #121212;">
    <div style="text-align: center">
        <h1 style="font-size: 50px; color: #fff; margin-top: 0; padding: 20px; background-color: #006494">Welcome!</h1>
        <p style="font-size: 22px">We are glad to have you! There is only one step left before you are ready to go!</p>
        <p style="font-size: 22px">Click the button below to validate your account.</p>
        <a style="font-size: 24px; margin: 15px; padding: 15px; border-radius: 5px; background-color: #006494; color: white; display: inline-block; text-decoration: none;" href="https://htmlhigh5.com/verifyemail/{{$email_token}}" target="_blank">Validate My Account!</a>
        <p style="font-size: 18px; margin-top: 10px;">If you are having trouble clicking the link, you may copy and paste this text into your browser: https://htmlhigh5.com/verifyemail/{{$email_token}}</p>
        <p style="margin-top: 50px;">&copy;{{\Carbon\Carbon::now()->year}} <a target="_blank" href="https://htmlhigh5.com/" title="Free HTML5 Games">HTML High 5</a>. All Rights Reserved.</p>
        <div style="background-color: #006494; padding: 20px; text-align: center">
            <img style="max-width: 100%" src="https://htmlhigh5.com/images/logo_menu_medium.png" alt="Logo">
        </div>
    </div>
</body>
</html>