@extends('layouts.app')
@section('title') Happy Birthday Esther! @endsection
@section('scripts')
<link type="text/css" rel="stylesheet" href="/css/chessboard-0.3.0.min.css">
    <script src="/js/chessboard-0.3.0.min.js"></script>
@endsection
@section('content')
    <div id="birthdayContainer">
        <h1 id="title">Happy Birthday!</h1>
        <p class="description">
            Happy birthday! To see your birthday card you must first find the unique Esther in the crowd of Esther faces. Have fun!
        </p>
        <div id="faceContainer">
            @for($i = 0, $values=['one', 'four', 'three', 'two', 'one', 'two','three','four','five','']; $i < 10; $i++)
                @for($n=0;$n<10;$n++)
                    @if($n != 4 || $i != 7)
                        <img class="hiddenImage {{$values[array_rand($values,2)[0]]}}" src="/images/birthday/esther.png">
                    @else
                        <img class="hiddenImage special" src="/images/birthday/esther.png">
                    @endif
                @endfor
                <br/>
            @endfor
        </div>
        <div id="chessBoardContainer">
            <div id="board1" style="width: 400px"></div>
        </div>
        <div id="codeContainer">
            <input type="text" title="code" name="code" id="code" class="codeInput">
            <input type="submit" value="Submit Code" id="submit" class="codeInput">
        </div>
        <a id="videoLink" href="https://www.youtube.com/watch?v=Eg_Rv2MmmCc" target="_blank">Watch The Secret Video</a>
    </div>
    <script>
        $('.special').click(()=>{
            $('#faceContainer').hide();
            $('.description').text('You solved the mystery of the weird floating Esther faces! Now you must solve the chess puzzle of doom (hint: distract the queen)');
            $('#title').text('Congratulations!');
            $('#chessBoardContainer').show();
        })
    </script>
    <script>
        var currentMove = 0;

        function pieceTheme(piece) {
            return '/images/chess/img/chesspieces/wikipedia/' + piece + '.png';
        }

        $('#submit').click(()=>{
            var code = $('#code');
            console.log('Code value: ', code.val());
            if(code.val() === 'Haykm'){
                $('#title').text('SECRET MESSAGE: ');
                $('.description').text('Here is the secret message! This video will teach you everything you need to know about surviving in the wilderness');
                $('#codeContainer').hide();
                $('#videoLink').show();
            }else{
                if(code.val() === 'haykm')
                    alert('Make sure you watch the capitalization!');
                else
                    alert('Incorrect!');
                code.val('');
            }
        });

        function onDrop(source, target, piece, newPos, oldPos, orientation) {
            if (currentMove === 0 && source === 'e1' && target === 'e8') {
                console.log('Way to go!');
                currentMove++;
                board.move('g8-e8');
                return 'trash';
            } else if(currentMove === 1 && source === 'g3' && target === 'g7'){
                setTimeout(()=>{
                    $('.description').text('You have solved the impossible challenges. Now you can receive the secret message. First, enter the code from your birthday card. To find the code, take the first letter from each line with a * at the beginning.');
                    $('#title').text('Happy Birthday Esther!!!');
                    $('#chessBoardContainer').hide();
                    $('#codeContainer').show();
                }, 50);
            } else{
                setTimeout(()=>{
                    board.position('6qk/8/5P1p/8/8/6QP/5PP1/4R1K1', false);
                    currentMove = 0;
                }, 50);
            }
        }

        var cfg = {
            pieceTheme: pieceTheme,
            position: '6qk/8/5P1p/8/8/6QP/5PP1/4R1K1',
            draggable: true,
            onDrop: onDrop
        };
        var board = ChessBoard('board1', cfg);
    </script>
@endsection