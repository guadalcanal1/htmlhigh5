@extends('layouts.app')
@section('title')FAQ @endsection
@section('content')
    <section id="faqContent">
        <h1>Frequently Asked Questions</h1>
        <p id="intro">
            Hopefully these questions will be helpful to you! If you have additional questions not answered here,
            please don't hesitate to <a href="/contact">contact us</a>!
        </p>
        <h2>How can I save my high scores?</h2>
        <p>
            To save your high scores, earn trophies, and track your statistics, you can
            <a href="/register">make an account</a> free of charge!
        </p>
        <h2>Do you track my personal information?</h2>
        <p>
            No. We do not track any personal information, and your email address will never be sold or given away. We
            keep vigilant security to ensure that your account information cannot be accessed by others.
        </p>
        <h2>Is my password safe if I make an account with you?</h2>
        <p>
            Yes! We use trusted encryption algorithms and industry standard practices to keep your information safe.
            In the unforeseen event of our server being hacked, your password would still be secure.
        </p>
        <h2>Does anyone read the FAQ?</h2>
        <p>
            Apparently you do. Besides you, the observer, it's hard to say. That's where things get philosophical.
        </p>
    </section>
@endsection