@extends('layouts.admin')

@section('title') Modify Game @endsection
@php
    //Might not be necessary, but can't hurt.
    if(!Auth::user() || !Auth::user()->is_admin){
        return redirect('/');
    }
@endphp
@section('content')
    <div class="adminFormContainer">
        <h1>Modify a Game</h1>
        <form action="/game/update/{{$game->slug}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <label>Name</label><br />
            <input title="Name" type="text" name="name" value="{{$game->name}}"><br />

            <label>Description</label><br />
            <textarea title="Description" name="description">{{$game->description}}</textarea><br />

            <label>Genre</label><br />
            <select title="Genre" name="genre">
                @foreach(\App\GameGenre::all() as $genre)
                    <option @if($game->genre_id == $genre->id) selected @endif value="{{$genre->id}}">{{$genre->name}}</option>
                @endforeach
            </select><br />

            <label>Keywords</label><br />
            <input title="Keywords" type="text" name="keywords" value="{{$game->keywords}}"><br />

            <h2>Game Settings</h2>

            <label>Max Possible Score</label><br />
            <input title="Max Score" type="number" name="max_score" value="{{$settings->max_score}}"><br />

            <label>Max Possible Score Increment</label><br />
            <input title="Max Score Increment" type="number" name="max_score_increment" value="{{$settings->max_score_increment}}"><br />

            <label>Score Increment Time</label><br />
            <input title="Score Increment Time" type="number" name="score_increment_time" value="{{$settings->score_increment_time}}"><br />

            <label>Max Time Spent</label><br />
            <input title="Max Time Spent" type="number" name="max_time_spent" value="{{$settings->max_time_spent}}"><br />

            <input type="submit" value="Submit">
        </form>
    </div>
@endsection