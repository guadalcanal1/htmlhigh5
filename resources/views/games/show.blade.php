@extends('layouts.app')
@section('title') {{$game->name}} @endsection
@section('tags')
    <meta property="og:title" content="{{$game->name}}">
    <meta property="og:type" content="article">
    <meta property="og:image" content="{{$game->cover_image_id->src}}">
    <meta property="og:author" content="Ian Andersen">
    <meta property="og:url" content="https://htmlhigh5.com/game/{{$game->slug}}">
    <meta property="og:description" content="{{$game->description}}">
    <meta property="og:site_name" content="HTML High 5">
    <meta name="description" content="{{$game->description}}">
    <meta name="keywords" content="{{$game->keywords}}">
    <meta property="fb:app_id" content="751998494959730">
@endsection
@section('content')
    <?php
            use App\User;
            use App\Game;
            ?>
    {{--{{User::updateAllPercentiles()}}--}}
    {{--{{Game::updateAllStats()}}--}}
    <div id="showGame">
        <section class="col-md-12" id="gameContainer">
            <h1 id="gameTitle">{{$game->name}}</h1>
            <div id="previewBackdrop" class="col-md-12">
                <div id="preview" class="col-md-offset-4 col-md-4">
                    <img id="highlightImage" src="../{{$game->cover_image_id->src}}" alt="{{$game->cover_image_id->alt_text}}">
                    <div class="actionButtonContainer">
                        @if(Auth::user())
                            <a class="actionButton" target="_blank" id="play" href="/play/{{$game->slug}}"><i class="icon-play-circle"></i> Play Now</a>
                        @else
                            <a id="play" class="actionButton" target="_blank" href="/play/{{$game->slug}}"><i class="icon-play-circle"></i> Play as Guest</a>
                            <div id="separator"></div>
                            <a id="register" class="actionButton" href="{{route('register')}}">Login or Register</a>
                        @endif
                    </div>
                </div>
                @if(Auth::user())
                    <div id="playerStats" class="col-md-4">
                        <h3>Your Stats:</h3>
                        <h4>Plays: {{Auth::user()->playCount($game->id)}}</h4>
                        <h4>Time Spent: {{Auth::user()->timeSpentOn($game->id)}}</h4>
                        <h4>Average Score: {{round(Auth::user()->averageScore($game->id) * 100) / 100}}</h4>
                        <h4>High Score: {{Auth::user()->highScore($game->id)}}</h4>
                        <h4>Percentile: {{round(Auth::user()->gamePercentile($game->id)  * 10000) / 100 . '%'}}</h4>
                        <div class="percentileBarContainer">
                            <div class="percentileBar" style="width: {{Auth::user()->gamePercentile($game->id) * 100 . '%'}};"></div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-md-4">
                <article id="gameDescription" class="col-md-12">
                    <h2>Description: </h2>
                    <p>{{$game->description}}</p>
                </article>
                @if($game->trophies()->get() && $game->trophies()->count() > 0)
                <div class="col-md-12">
                    <h3>Trophies:</h3>

                        @foreach($game->trophies()->get() as $trophy)
                            <div class="trophyContainerLarge" title="{{$trophy->description}}">
                                <img class="trophyImageLarge" src="{!! $trophy->image->src !!}" alt="{{$trophy->name}}">
                                @if(!Auth::user() || !Auth::user()->hasUnlockedTrophy($trophy->id))
                                    <div class="trophyImageLock">
                                        <i class="icon-lock"></i>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                </div>
                @endif
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Game Page Ad -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:336px;height:280px"
                     data-ad-client="ca-pub-5446454508055586"
                     data-ad-slot="4704428757"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
            <div id="gameProps" class="col-md-4">
                <div id="ratings">
                <h3>Overall Rating: {{$rating}}</h3>
                <div class="ratingStars">
                    @for($i = 0; $i < 5; $i ++)
                        @if($i <= $rating-1)
                            <i class="icon-star"></i>
                        @elseif($i < $rating)
                            <i class="icon-star-half-empty"></i>
                        @else
                            <i class="icon-star-empty"></i>
                        @endif
                    @endfor
                </div>

                    @if(Auth::user())
                    <h3>Your Rating:</h3>
                    <div class="ratingStars">
                        <a val=1 onclick="rate(1)" class="icon-star userRating"></a>
                        <a val=2 onclick="rate(2)" class="icon-star userRating"></a>
                        <a val=3 onclick="rate(3)" class="icon-star userRating"></a>
                        <a val=4 onclick="rate(4)" class="icon-star userRating"></a>
                        <a val=5 onclick="rate(5)" class="icon-star userRating"></a>
                    </div>
                    @endif
                </div>
                <div id="stats">
                    <h3>Total Plays: {{$playCount}}</h3>
                    <h3>Time Spent: {{$timeSpent}}</h3>
                    <h3>Average Score: {{$averageScore}}</h3>
                </div>
            </div>
            <div class="topTable col-md-4">
                <table>
                    <caption>Top Scorers</caption>
                    <tr><th>User</th><th>Score</th></tr>
                    @if($topScores)
                        @foreach($topScores as $top)
                            @if($top->user_id > 0)
                                @include('partials.userTableRow', [
                                        'user' => User::find($top->user_id),
                                        'value' => $top->value
                                    ])
                            @endif
                        @endforeach
                    @endif
                </table>
            </div>
        </section>
        <section id="gameStats" class="col-md-12">
            <div class="col-md-10 col-md-offset-1 graphContainer">
                <canvas id="playsThisWeek"></canvas>
            </div>
            <script>
                $('#play').click(function(){
                    $.post('/buttonClick/{{$game->slug}}');
                })
            </script>
            <script>
                const scores = {!! json_encode($scoreGroups) !!};
                let values = [];
                let labels = [];
                for(let i = 0; i < scores.length; i++){
                    values[i] = scores[i].count;
                    labels[i] =  scores[i].range;
                }

                let weeklyPlaysCanvas = $("#playsThisWeek");
                let scoreChart = new Chart(weeklyPlaysCanvas, {
                    type: 'line',
                    data: {
                        labels: labels,
                        datasets: [{
                            label: 'Score Distribution',
                            data: values,
                            backgroundColor: 'rgba(255,255,255,.3)',
                            borderColor: 'white',
                            borderWidth: 3
                        }]
                    },
                    options: {
                        legend: {labels:{fontColor:"white", fontSize: 18}},
                        scales: {
                            yAxes: [{
                                ticks: {
                                    fontColor: 'white',
                                    beginAtZero:true
                                }
                            }],
                            xAxes: [{
                                ticks: {
                                    fontColor: 'white'
                                }
                            }]
                        },
                        responsive: true,
                        scaleFontColor: "#FFFFFF",
                        pointLabelFontColor: "#FFFFFF"
                    }
                });

                function rate(value){
                    userRating = value;
                    $.post( "/rate", {
                        'rating': value,
                        'game': {{$game->id}},
                        "_token": "{{ csrf_token() }}"
                    },
                    function(data) {
                        if(data != userRating)
                            console.log(data);
                    });
                }

                @if(Auth::user())
                    let userRating = {{$userRating}};
                    $(".userRating").hover(function(){
                        let hoveredValue = $(this).attr('val');
                        $('.userRating').each(function () {
                            let val = $(this).attr('val');
                            if (val > hoveredValue) {
                                $(this).addClass('icon-star-empty');
                                $(this).removeClass('icon-star');
                            }else{
                                $(this).removeClass('icon-star-empty');
                                $(this).addClass('icon-star');
                            }
                        });
                    }, function(){
                        showRating();
                    });

                    function showRating(){
                        $('.userRating').each(function () {
                            let val = $(this).attr('val');
                            if (val > userRating) {
                                $(this).addClass('icon-star-empty');
                                $(this).removeClass('icon-star');
                            }else{
                                $(this).removeClass('icon-star-empty');
                                $(this).addClass('icon-star');
                            }
                        });
                    }
                showRating();
                @endif
            </script>
        </section>
    </div>
    <div id="gameRatingsContainer">
         <span>
             Rated <span itemprop="ratingValue">{{$rating}}</span> / 5 based on <span itemprop="reviewCount">{{$numRatings}}</span> reviews. | <a class="ratings" href="#gameProps">Review Me</a>
         </span>
    </div>
    <div id="gameTags">
        <h3>Tags</h3>
        @foreach($keywords as $keyword)
            <div class="keywordBox">
                <h3>{{$keyword}}</h3>
            </div>
        @endforeach
    </div>
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "VideoGame",
      "@id": "https://htmlhigh5.com/game/{{$game->slug}}",
      "name": "{{$game->name}}",
      "image": "../{{$game->cover_image_id->src}}",
      "sameAs": "https://htmlhigh5.com/play/{{$game->slug}}",
      "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "{{$rating}}",
        "bestRating": "5",
        "worstRating": "1",
        "ratingCount": "{{$numRatings}}"
      },
      "applicationCategory": "Game",
      "description": "{{$game->description}}",
      "genre": "{{$game->genre ? $game->genre->name : 'HTML5'}}",
      "operatingSystem": "Windows 10, iOS, Android, OS X, Linux, Ubuntu, Chrome OS",
      "url": "http://htmlhigh5.com/game/{{$game->slug}}"
    }
    </script>
@endsection