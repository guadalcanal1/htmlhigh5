@extends('gotcha.layout')

@section('content')
    <div style="margin-top: 80px; background-color: white;" class="col-md-12">
        <form id="choosePlayerForm" class="waldockForm" action="/waldock/selectPlayer" method="POST">
            <label>Player</label><br/>
            <select title="Player" name="player">
                @foreach($allPlayers as $p)
                    <option value="{{$p->id}}">{{$p->name}}</option>
                @endforeach
            </select>
            <br/>
            <input type="submit" value="Submit">
        </form>
    </div>
@endsection