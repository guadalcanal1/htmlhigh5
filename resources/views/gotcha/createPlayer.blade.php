@extends('gotcha.layout')

@section('content')
    <div style="margin-top: 80px; background-color: white;" class="col-md-12">
        <form id="newPlayerForm" class="waldockForm" action="/waldock/createPlayer" method="POST">
            <label>Name</label>
            <br/>
            <input type="text" name="name" title="Name" required="required">
            <br/>
            <label>Dorm</label>
            <br/>
            <select title="Dorm" name="dorm" required="required">
                <option value="Unknown">Unknown</option>
                <option value="Waldock">Waldock</option>
                <option value="Dixon">Dixon</option>
                <option value="CDub">CDub</option>
                <option value="Sweazy">Sweazy</option>
                <option value="Hotchkiss">Hotchkiss</option>
                <option value="Slight">Slight</option>
                <option value="Off Campus">Off Campus</option>
                <option value="Faculty">Faculty</option>
            </select>
            <br/>
            <label>Target</label>
            <br/>
            <select title="Target" name="target">
                <option value="0">Unknown</option>
                @foreach($allPlayers as $p)
                    <option value="{{$p->id}}">{{$p->name}}</option>
                @endforeach
            </select>
            <br/>
            <label>Targeted By</label>
            <br/>
            <select title="Targeted By" name="targetedBy">
                <option value="0">Unknown</option>
                @foreach($allPlayers as $p)
                    <option value="{{$p->id}}">{{$p->name}}</option>
                @endforeach
            </select>
            <br/>
            <label>Additional Info</label>
            <br/>
            <textarea title="More Info" name="moreInfo"></textarea>
            <br/>
            <label>Password</label>
            <br/>
            <input type="password" autocomplete="off" name="password" title="Password" required="required">
            <br/>
            <input type="submit" value="Submit">
        </form>
    </div>
@endsection