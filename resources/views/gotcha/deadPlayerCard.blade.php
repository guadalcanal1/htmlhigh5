<div class="playerCard dead col-md-3">
    <h3 class="playerName"><span>{{$player->name}}</span></h3>
    <h4 class="playerDorm">Dorm: <span>{{$player->dorm}}</span></h4>
    <p class="playerTarget emphasized">Target: <span>{{$player->target ? $player->target->name : "Unknown"}}</span></p>
    <p class="playerTargetedBy emphasized">Targeted By: <span>{{$player->targeted_by ? $player->targeted_by->name : "Unknown"}}</span></p>
</div>