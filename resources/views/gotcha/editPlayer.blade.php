@extends('gotcha.layout')

@section('content')
    <div style="margin-top: 80px; background-color: white;" class="col-md-12">
        <form id="editPlayerForm" class="waldockForm" action="/waldock/updatePlayer/{{$player->id}}" method="POST">
            <label>Name</label>
            <br/>
            <input type="text" name="name" title="Name" required="required" value="{{$player->name}}">
            <br/>
            <label>Dorm</label>
            <br/>
            <select title="Dorm" name="dorm" required="required">
                <option {{$player->dorm === "Unknown" ? "selected" : ""}} value="Unknown">Unknown</option>
                <option {{$player->dorm === "Waldock" ? "selected" : ""}} value="Waldock">Waldock</option>
                <option {{$player->dorm === "Dixon" ? "selected" : ""}} value="Dixon">Dixon</option>
                <option {{$player->dorm === "CDub" ? "selected" : ""}} value="CDub">CDub</option>
                <option {{$player->dorm === "Sweazy" ? "selected" : ""}} value="Sweazy">Sweazy</option>
                <option {{$player->dorm === "Hotchkiss" ? "selected" : ""}} value="Hotchkiss">Hotchkiss</option>
                <option {{$player->dorm === "Slight" ? "selected" : ""}} value="Slight">Slight</option>
                <option {{$player->dorm === "Off Campus" ? "selected" : ""}} value="Off Campus">Off Campus</option>
                <option {{$player->dorm === "Faculty" ? "selected" : ""}} value="Faculty">Faculty</option>
            </select>
            <br/>
            <label>Target</label>
            <br/>
            <select title="Target" name="target">
                <option value="0">Unknown</option>
                @foreach($allPlayers as $p)
                    <option value="{{$p->id}}" {{$player->target ? $player->target->id === $p->id ? "selected" : "" : ""}}>{{$p->name}}</option>
                @endforeach
            </select>
            <br/>
            <label>Targeted By</label>
            <br/>
            <select title="Targeted By" name="targetedBy">
                <option value="0">Unknown</option>
                @foreach($allPlayers as $p)
                    <option value="{{$p->id}}" {{$player->targetedBy ? $player->targetedBy->id === $p->id ? "selected" : "" : ""}}>{{$p->name}}</option>
                @endforeach
            </select>
            <br/>
            <label>Additional Info</label>
            <br/>
            <textarea title="More Info" name="moreInfo">{{$player->moreInfo}}</textarea>
            <br/>
            <label>Password</label>
            <br/>
            <input type="password" autocomplete="off" name="password" title="Password" required="required">
            <br/>
            <input type="submit" value="Submit">
        </form>
    </div>
@endsection