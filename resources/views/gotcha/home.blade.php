@extends('gotcha.layout')

@section('content')
    <div style="margin-top: 80px; background-color: white;" class="col-md-12">
        <div class="col-md-12" id="adminButtons">
            <a href="/waldock/newPlayer">Add new player</a>
            <br/>
            <a href="/waldock/choosePlayer">Edit an existing Player</a>
        </div>
        <div class="col-md-12">
            @if(count($chains))
                @foreach($chains as $chain)
                    @if(count($chain))
                        <h3>Chain Length: {{count($chain)}}</h3>
                        @foreach($chain as $w)
                            {{$w->name}} -->
                        @endforeach
                        <br/>
                    @endif
                @endforeach
            @endif
        </div>
        <div class="col-md-4 homeColumn">
            <h2>People to avoid</h2>
            @foreach($waldockTargetedBy as $w)
                @include('gotcha.playerCard', [
                    'player' => $w,
                    'allPlayers' => $allPlayers,
                    'i' => 1
                ])
            @endforeach
        </div>
        <div class="col-md-4 homeColumn">
            <h2>Our people</h2>
            @foreach($waldockPlayers as $w)
                @include('gotcha.playerCard', [
                    'player' => $w,
                    'allPlayers' => $allPlayers,
                    'i' => 2
                ])
            @endforeach
        </div>
        <div class="col-md-4 homeColumn">
            <h2>People to find</h2>
            @foreach($waldockTargets as $w)
                @include('gotcha.playerCard', [
                    'player' => $w,
                    'allPlayers' => $allPlayers,
                    'i' => 3
                ])
            @endforeach
        </div>
        <div class="col-md-12" id="killedPlayers">
            <h2 style="color: red">Dead Players</h2>
            @foreach($killedPlayers as $w)
                @include('gotcha.deadPlayerCard', [
                    'player' => $w
                ])
            @endforeach
        </div>
    </div>
@endsection