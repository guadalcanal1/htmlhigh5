<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('partials.icons')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Waldock Gotcha - @yield('title')</title>
@yield('tags')

<!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('css/template/main.css')}}" rel="stylesheet">
    <link href="{{asset('css/template/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/template/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('css/template/animate.css')}}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <script src="{{asset('js/html5shiv.js')}}"></script>
    <script src="{{asset('js/respond.min.js')}}"></script>
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
@yield('scripts')
<!--{{\App\PageLoad::create([
        'user_id' => \App\Http\Controllers\UserController::getUserID(),
        'info' => Request::url()
    ])}}-->
</head>
<body style="max-width:100%" id="waldock">
<header class="navbar navbar-inverse navbar-fixed-top nav-primary" style="height: 80px">
    <div class="container" style="width: 100%">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img alt="HTML High 5 Logo" src="/images/logo_menu_medium.png"></a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav" style="margin-left: 35px;">
                <li><a href="/">HTML High 5</a></li>
                <li><a href="/waldock">Home</a></li>
                <li><a href="/waldock/newPlayer">Create Player</a></li>
                <li><a href="/waldock/choosePlayer">Edit Player</a></li>
            </ul>
        </div>
    </div>
</header>

@yield('content')

<footer id="footer" class="midnight-blue">
    <div class="container">
        <div class="row">
            <div class="col-md-12" id="siteDescription">
                <a href="/">HTML High 5</a> is a free website for enjoyable, high quality online browser games. All games are
                mobile friendly and free, and if you make an account
                you can save your high scores, view your statistics, and unlock trophies. Enjoy our HTML5 games!
            </div>
        </div>
    </div>
</footer><!--/#footer-->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-79189593-1', 'auto');
    ga('require', 'linkid');
    ga('send', 'pageview');

</script>
<script>
    $('.commentButton').click(function(e){
        const playerID = e.target.id.split('-')[1];
        const num = e.target.id.split('-')[2];
        const form = $('#form-' + playerID + "-" + num);
        $.post('/waldock/addComment', {
            comment: form.val(),
            id: playerID
        }).done(function(data){
            const info = $('#info-' + playerID + "-" + num);
            info.html(data);
        });
    });
    $('.killButton').click(function(e){
        const playerID = e.target.id.split('-')[1];
        const num = e.target.id.split('-')[2];
        const input = $('#delete-' + playerID + "-" + num);
        const killer = $('#killers-' + playerID + "-" + num).val();
        $.post('/waldock/killPlayer', {
            id: playerID,
            password: input.val(),
            killerID: killer
        }).done(function(data){
            location.reload();
        });
    });
</script>
</body>
</html>
