<div class="playerCard {{$player->is_waldock ? "waldock" : ""}}">

    <label>Killer: </label><select title="Killer" id="killers-{{$player->id}}-{{$i}}">
        @foreach($allPlayers as $p)
            @if($p->id !== $player->id)
                <option value="{{$p->id}}">{{$p->name}}</option>
            @endif
        @endforeach
    </select>
    <label>Password: </label><input title="Killed" type="password" autocomplete="off" id="delete-{{$player->id}}-{{$i}}">
    <button id="killButton-{{$player->id}}-{{$i}}" class="killButton num-{{$i}}">Player Killed</button>


    <h3 class="playerName"><span>{{$player->name}}</span></h3>
    <h4 class="playerDorm">Dorm: <span>{{$player->dorm}}</span></h4>
    <p class="playerTarget emphasized">Target: <span>{{$player->target ? $player->target->name : "Unknown"}}</span></p>
    <p class="playerTargetedBy emphasized">Targeted By: <span>{{$player->targeted_by ? $player->targeted_by->name : "Unknown"}}</span></p>
    <p class="moreInfo">More Info: <br/> <span id="info-{{$player->id}}-{{$i}}">{!! $player->more_info !!}</span></p>
    <label>More Info on {{$player->name}}: </label>
    <br/>
    <textarea id="form-{{$player->id}}-{{$i}}" title="Comment" class="addComment num-{{$i}}"></textarea>
    <br/>
    <button id="button-{{$player->id}}-{{$i}}" class="commentButton num-{{$i}}">Submit</button>
</div>