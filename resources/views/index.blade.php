@extends('layouts.app')
@section('title') Free HTML5 Games @endsection
@section('tags')
    <meta property="og:title" content="HTML High 5">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://htmlhigh5.com/images/logo_square.png">
    <meta property="og:author" content="Ian Andersen">
    <meta property="og:url" content="https://htmlhigh5.com/}">
    <meta property="og:description" content="Play free, high quality HTML5 games on HTML High5. Make an account to save your scores and rule the leader boards. Do you have what it takes to be the best?">
    <meta property="og:site_name" content="HTML High 5">
    <meta name="description" content="Play free, high quality HTML5 games on HTML High5. Make an account to save your scores and rule the leader boards. Our online games are mobile friendly and fun!">
    <meta property="fb:app_id" content="751998494959730">
@endsection
@section('scripts')
    <script src="js/jssor.slider-25.2.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {

            var jssor_1_options = {
                $AutoPlay: 1,
                $SlideWidth: 640,
                $Cols: 2,
                $Align: 170,
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/
            /*remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 980);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        });
    </script>
    <style>
        /* jssor slider loading skin double-tail-spin css */

        .jssorl-004-double-tail-spin img {
            animation-name: jssorl-004-double-tail-spin;
            animation-duration: 1.2s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-004-double-tail-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        .jssorb051 .i {position:absolute;cursor:pointer;}
        .jssorb051 .i .b {fill:#fff;fill-opacity:0.5;stroke:#000;stroke-width:400;stroke-miterlimit:10;stroke-opacity:0.5;}
        .jssorb051 .i:hover .b {fill-opacity:.7;}
        .jssorb051 .iav .b {fill-opacity: 1;}
        .jssorb051 .i.idn {opacity:.3;}

        .jssora051 {display:block;position:absolute;cursor:pointer;}
        .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
        .jssora051:hover {opacity:.8;}
        .jssora051.jssora051dn {opacity:.5;}
        .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
    </style>
@endsection
@section('content')
    <div id="homeContainer" class="col-md-12">
        <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-004-double-tail-spin" style="position:absolute;top:0px;left:0px;text-align:center;background-color:rgba(0,0,0,0.7);">
                <img alt="loading animation" style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="images/slider/double-tail-spin.svg" />
            </div>
            <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:400px;overflow:hidden;">
                @for ($i = 0; $i < count($games); $i++)
                    <div>
                        <a id="slide_{{$games[$i]->slug}}" href="{{'game/' . $games[$i]->slug}}">
                            <img data-u="image" id="game_{{$i}}"  src="{{$games[$i]->cover_image_id->src}}" alt="{{$games[$i]->cover_image_id->alt_text}}">
                        </a>
                    </div>
                @endfor
            </div>
            <!-- Bullet Navigator -->
            <div data-u="navigator" class="jssorb051" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                <div data-u="prototype" class="i" style="width:16px;height:16px;">
                    <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                    </svg>
                </div>
            </div>
            <!-- Arrow Navigator -->
            <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:45px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:45px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                </svg>
            </div>
        </div>
        <!-- #endregion Jssor Slider End -->
        <div class="boxAd">
            {{--<p class="adBlock">Please disable AdBlock to keep our site free for everyone!</p>--}}
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Homepage Responsive -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-5446454508055586"
                 data-ad-slot="4939643137"
                 data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <section id="gameList" class="col-md-12">
            <h2 id="allGamesTitle">All Games</h2>
            @for ($i = 0; $i < count($allGames); $i++)
                <a id="list_{{$allGames[$i]->slug}}" href="game/{{$allGames[$i]->slug}}">
                    <div class="smallGamePreview">
                        <div class="box">
                            <img src="{{$allGames[$i]->cover_image_id->src}}" alt="{{$allGames[$i]->cover_image_id->alt_text}}">
                            <h3>{{$allGames[$i]->name}}</h3>
                        </div>
                    </div>
                </a>
            @endfor
        </section>
        <p id="informationText">At HTML High 5 you can find the best free online games on the internet. All games work on mobile phones in
        the browser for ultimate compatibility. All our online games are fun, free, and high quality. Thanks to the high score leader boards,
        you can track your progress with ease and aim to be the best on the site. Good luck!</p>
    </div>
    <script>

        setTimeout(function (){
            $('.adBlock').show();
        }, 3000);
    </script>
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "VideoGameSeries",
      "@id": "https://htmlhigh5.com",
      "name": "HTML High 5",
      "image": "https://htmlhigh5.com/images/logo_menu_medium.png",
      "sameAs": "http://htmlhigh5.com",
      "description": "Play free, high quality HTML5 games on HTML High5. Make an account to save your scores and rule the leader boards. Our online games are mobile friendly and fun!",
      "genre": "HTML5",
      "url": "http://htmlhigh5.com"
    }
    </script>
@endsection