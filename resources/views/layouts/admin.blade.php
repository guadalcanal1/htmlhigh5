<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex" />
    @include('partials.icons')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>HTML High 5 - @yield('title')</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('css/template/main.css')}}" rel="stylesheet">
    <link href="{{asset('css/template/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/template/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('css/template/animate.css')}}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <script src="{{asset('js/html5shiv.js')}}"></script>
    <script src="{{asset('js/respond.min.js')}}"></script>
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
</head>
<body id="adminBody">
<header class="navbar navbar-inverse navbar-fixed-top peter-river" style="height: 80px" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">HTML High 5</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="/">Home</a></li>
                <li>
                    <a href="/">Search Games</a>
                </li>
                <li><a href="/contact">Contact</a></li>
                @if (Auth::guest())
                    <li class="menuEmphasized"><a href="{{ route('login') }}">Login</a></li>
                    <li class="menuEmphasized"><a href="{{ route('register') }}">Register</a></li>
                @else
                    <li class="menuEmphasized">
                        <a href="/user/{{Auth::user()->slug}}" role="button" aria-expanded="false">
                            {{ Auth::user()->name }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</header>

@yield('content')

<footer id="footer" class="belize-hole">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                &copy; 2017 <a target="_blank" href="https://htmlhigh5.com/" title="Free HTML5 Games">HTML High 5</a>. All Rights Reserved.
            </div>
            <div class="col-sm-6">
                <ul class="pull-right">
                    <li><a href="/">Home</a></li>
                    <li><a href="/about">About Us</a></li>
                    <li><a href="/faq">Faq</a></li>
                    <li><a href="/contact">Contact Us</a></li>
                    <li><a id="gototop" class="gototop" href="#"><i class="icon-chevron-up"></i></a></li><!--#gototop-->
                </ul>
            </div>
        </div>
    </div>
</footer><!--/#footer-->

</body>
</html>
