<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('partials.icons')
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1828975317432831',
                xfbml      : true,
                version    : 'v2.10'
            });
            FB.AppEvents.logPageView();
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>HTML High 5 - @yield('title')</title>
    @yield('tags')

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('css/template/main.css')}}" rel="stylesheet">
    <link href="{{asset('css/template/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/template/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('css/template/animate.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/social/jssocials.css" />
    <link rel="stylesheet" type="text/css" href="/css/social/jssocials-theme-flat.css" />
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <script src="{{asset('js/html5shiv.js')}}"></script>
    <script src="{{asset('js/respond.min.js')}}"></script>
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
    @yield('scripts')
<!--{{\App\PageLoad::create([
        'user_id' => \App\Http\Controllers\UserController::getUserID(),
        'info' => Request::url()
    ])}}-->
</head>
<body style="max-width:100%">
    <header class="navbar navbar-inverse navbar-fixed-top nav-primary" style="height: 80px">
        <div class="container" style="width: 100%">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img alt="HTML High 5 Logo" src="/images/logo_menu_medium.png"></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav" style="margin-left: 35px;">
                    <li style="padding-bottom: 5px;">
                        <a href="https://twitter.com/htmlhigh5" class="btn-social btn-twitter" rel="nofollow noopener" target="_blank">
                            <i class="icon-twitter"></i>
                        </a>
                        <a href="https://www.facebook.com/htmlhigh5" class="btn-social btn-facebook" rel="nofollow noopener" target="_blank">
                            <i class="icon-facebook"></i>
                        </a>
                        <a href="https://plus.google.com/+HTMLHighFive" class="btn-social btn-google-plus" rel="nofollow noopener" target="_blank">
                            <i class="icon-google-plus"></i>
                        </a>
                        <a href="https://www.instagram.com/htmlhigh5/" class="btn-social btn-instagram" rel="nofollow noopener" target="_blank">
                            <i class="icon-instagram"></i>
                        </a>
                    </li>
                    <li class="menuItem"><a href="/">Home</a></li>
                    <li class="menuItem">
                        <a href="/leaderBoards">Ranks</a>
                    </li>
                    {{--<li class="menuItem"><a href="/contact">Contact</a></li>--}}
                    @if (Auth::guest())
                        <li class="menuEmphasized menuItem"><a href="{{ route('login') }}">Login</a></li>
                        <li class="menuEmphasized menuItem"><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="menuEmphasized menuItem">
                            <a href="/user/{{Auth::user()->slug}}" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} ({{Auth::user()->percentileStats() ? round(Auth::user()->percentileStats()->overall_percentile * 1000) : 0}})
                            </a>
                        </li>
                        <li class="menuItem">
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </header>

    @yield('content')

    <footer id="footer" class="footer-secondary">
        <div class="container">
            <div class="row">
                <div class="col-md-12" id="siteDescription">
                    HTML High 5 is a free website for enjoyable, high quality online browser games. All games are
                    mobile friendly and free, and if you make an account
                    you can save your high scores, view your statistics, and unlock trophies. Enjoy our HTML5 games!
                </div>
                <div class="col-sm-12" style="text-align: center; padding-top: 10px;">
                    @include('partials.social')
                </div>
                <div class="col-sm-6">
                    &copy; {{\Carbon\Carbon::now()->year}} <a target="_blank" href="https://htmlhigh5.com/" title="Free HTML5 Games">HTML High 5</a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="/">Home</a></li>
                        <li><a href="/about">About Us</a></li>
                        <li><a href="/faq">Faq</a></li>
                        <li><a href="/contact">Contact Us</a></li>
                        <li><a href="/partners">Partners</a></li>
                        <li><a id="gototop" class="gototop" href="#"><i class="icon-chevron-up"></i></a></li><!--#gototop-->
                    </ul>
                </div>
                <div style="text-align: center" class="col-md-12">
                    <div
                            class="fb-like"
                            data-share="true"
                            data-width="450"
                            data-show-faces="true"
                            data-colorscheme="dark">
                    </div>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-79189593-1', 'auto');
        ga('require', 'linkid');
        ga('send', 'pageview');

    </script>
</body>
</html>
