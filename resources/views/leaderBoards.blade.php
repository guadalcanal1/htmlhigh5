@extends('layouts.app')
@section('title') High Score Leader Boards @endsection
@section('tags')
    <meta property="og:title" content="HTML High 5">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://htmlhigh5.com/images/logo_square.png">
    <meta property="og:author" content="Ian Andersen">
    <meta property="og:url" content="https://htmlhigh5.com/}">
    <meta property="og:description" content="Play free, high quality HTML5 games on HTML High5. Make an account to save your scores and rule the leader boards. Do you have what it takes to be the best?">
    <meta property="og:site_name" content="HTML High 5">
    <meta name="description" content="Play free, high quality HTML5 games on HTML High5. Make an account to save your scores and rule the leader boards. Do you have what it takes to be the best?">
    <meta property="fb:app_id" content="751998494959730">
@endsection
@section('scripts')
    <script></script>
@endsection
@section('content')
    <div id="leaderBoardContainer" class="col-md-12">
        <h1>Leader Boards</h1>
        <section id="tables">
            <h2 style="font-size: 32px; color: #2c3e50; text-align: center;">High Score leaderboards are updated every five minutes.</h2>
            <div class="topTable homepageTable col-md-3">
                <table>
                    <caption>Top Rated Players</caption>
                    <tr><th>User</th><th>High5 Rating</th></tr>
                    @foreach($topUsers as $top)
                        @include('partials.userTableRow', [
                            'user' => \App\User::find($top->user_id),
                            'value' => round($top->overall_percentile * 1000)
                        ])
                    @endforeach
                </table>
            </div>
            <div class="topTable homepageTable col-md-3">
                <table>
                    <caption>Most Dedicated Players</caption>
                    <tr><th>User</th><th>Play Count</th></tr>
                    @foreach($dedicatedUsers as $top)
                        @include('partials.userTableRow', [
                            'user' => \App\User::find($top->user_id),
                            'value' => $top->play_count
                        ])
                    @endforeach
                </table>
            </div>
            <div class="topTable homepageTable col-md-3">
                <table>
                    <caption>Most Decorated Players</caption>
                    <tr><th>User</th><th>Trophy Count</th></tr>
                    @foreach($decoratedUsers as $top)
                        @include('partials.userTableRow', [
                            'user' => \App\User::find($top->user_id),
                            'value' => $top->trophy_count
                        ])
                    @endforeach
                </table>
            </div>
            <div class="topTable homepageTable col-md-3">
                <table>
                    <caption>Top Rated Games</caption>
                    <tr><th>Game</th><th>Score</th></tr>
                    @foreach($bestGames as $top)
                        @include('partials.gameTableRow', [
                            'game' => \App\Game::find($top->game_id),
                            'value' => round($top->rating * 1000)
                        ])
                    @endforeach
                </table>
            </div>
            <div class="topTable homepageTable col-md-3">
                <table>
                    <caption>Most Played Games</caption>
                    <tr><th>Game</th><th>Play Count</th></tr>
                    @foreach($mostPlayedGames as $top)
                        @include('partials.gameTableRow', [
                            'game' => \App\Game::find($top->game_id),
                            'value' => round($top->play_count)
                        ])
                    @endforeach
                </table>
            </div>
            <div class="topTable homepageTable col-md-3">
                <table>
                    <caption>Time-Consuming Games</caption>
                    <tr><th>Game</th><th>Time Spent</th></tr>
                    @foreach($timeConsumingGames as $top)
                        @include('partials.gameTableRow', [
                            'game' => \App\Game::find($top->game_id),
                            'value' => \App\Game::find($top->game_id)->timeSpentString()
                        ])

                    @endforeach
                </table>
            </div>
        </section>
    </div>
@endsection