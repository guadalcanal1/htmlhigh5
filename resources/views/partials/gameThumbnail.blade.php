<div class="gameThumbnailContainer">
    <a href="/game/{{$game->slug}}">
        <img class="gameThumbnail" src="{{$game->thumbnail->src}}" alt="{{$game->name}} Thumbnail">
        @if(isset($link))
            {{$link}}
        @endif
    </a>
</div>