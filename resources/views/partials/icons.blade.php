<link rel="manifest" href="/manifest.json">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="application-name" content="High5">
<meta name="apple-mobile-web-app-title" content="High5">
<meta name="theme-color" content="#d35400">
<meta name="msapplication-navbutton-color" content="#d35400">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="msapplication-starturl" content="/">
<link rel="apple-touch-icon" sizes="57x57" href="https://htmlhigh5.com/images/icons/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="114x114" href="https://htmlhigh5.com/images/icons/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="72x72" href="https://htmlhigh5.com/images/icons/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="144x144" href="https://htmlhigh5.com/images/icons/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="60x60" href="https://htmlhigh5.com/images/icons/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon" sizes="120x120" href="https://htmlhigh5.com/images/icons/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="76x76" href="https://htmlhigh5.com/images/icons/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="152x152" href="https://htmlhigh5.com/images/icons/apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="https://htmlhigh5.com/images/icons/favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="https://htmlhigh5.com/images/icons/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="https://htmlhigh5.com/images/icons/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="https://htmlhigh5.com/images/icons/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="https://htmlhigh5.com/images/icons/favicon-128.png" sizes="128x128" />
<meta name="application-name" content="HTML High 5"/>
<meta name="msapplication-TileColor" content="#000000" />
<meta name="msapplication-TileImage" content="https://htmlhigh5.com/images/icons/mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="https://htmlhigh5.com/images/icons/mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="https://htmlhigh5.com/images/icons/mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="https://htmlhigh5.com/images/icons/mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="https://htmlhigh5.com/images/icons/mstile-310x310.png" />