@extends('layouts.app')
@section('title') Add the TMU Cafeteria Menu to Siri! @endsection
@section('content')
    <div id="shortcutsContainer">
        <h1>Add the Master's University Cafeteria Menu to Siri!</h1>
        <div id="steps">
            <div class="step">
                <h2 class="stepTitle">Step 1. Install the Shortcuts App</h2>
                <img class="shortcutImage" src="/images/caf/shortcuts_logo.jpg">
                <br/>
                <a href="https://itunes.apple.com/us/app/shortcuts/id915249334?mt=8" style="display:inline-block;overflow:hidden;background:url(https://linkmaker.itunes.apple.com/en-us/badge-lrg.svg?releaseDate=2014-12-11&kind=iossoftware&bubble=ios_apps) no-repeat;width:135px;height:40px;"></a>
                <p>This app came out with iOS 12, so make sure your phone is updated to the correct version!</p>
            </div>
            <div class="step">
                <h2 class="stepTitle">Step 2. Download the Shortcuts</h2>
                <a target="_blank" class="shortcut" href="https://www.icloud.com/shortcuts/ef5ff53538754520ae7850c0f036f603">
                    <img class="shortcutImage" src="/images/caf/breakfast.png">
                    <p class="shortcutName">Breakfast</p>
                </a>
                <br/>
                <a target="_blank" class="shortcut" href="https://www.icloud.com/shortcuts/fbddc238a6cc49c9817e66faae04b6cd">
                    <img class="shortcutImage" src="/images/caf/brunch.png">
                    <p class="shortcutName">Brunch</p>
                </a>
                <br/>
                <a target="_blank" class="shortcut" href="https://www.icloud.com/shortcuts/120538842e344026bc40615a32167522">
                    <img class="shortcutImage" src="/images/caf/lunch.png">
                    <p class="shortcutName">Lunch</p>
                </a>
                <br/>
                <a target="_blank" class="shortcut" href="https://www.icloud.com/shortcuts/84dfb0a727564cf98efe6b37285ef5c9">
                    <img class="shortcutImage" src="/images/caf/dinner.png">
                    <p class="shortcutName">Dinner</p>
                </a>
                <br/>
            </div>
            <div class="step">
                <h2 class="stepTitle">Step 3. Add the Shortcuts to Siri</h2>
                <div class="tutorial">
                    <img class="tutorialImage" src="/images/caf/tut_1.jpg">
                    <p>Go to the settings of the shortcut</p>
                    <img class="tutorialImage" src="/images/caf/tut_2.jpg">
                    <p>Press the Settings button</p>
                    <img class="tutorialImage" src="/images/caf/tut_3.jpg">
                    <p>Press "Add to Siri" and record the phrase you want to use!</p>
                </div>
            </div>

        </div>
    </div>
@endsection