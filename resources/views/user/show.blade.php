@extends('layouts.app')
@section('title') {{$user->name}} @endsection
@section('content')
    <div id="userProfile" class="col-md-12">
        <h1 id="profileTitle">{{$user->name}}</h1>
        <h2 id="profileHigh5Score">Rating: <span>{{round($percentile ? $percentile->overall_percentile * 1000 : 0)}}</span></h2>
        <h3 id="profileRanking">Rank: <span>{{$overallRating}}</span></h3>
        <div id="userStats" class="col-md-12">
            <div class="col-md-3 bubbleContainer">
                <div class="bubble">
                    <span class="attribute">{{$percentile ? $percentile->play_count : 0}}</span>
                    <br/>
                    <span class="description">Play Count</span>
                </div>
            </div>
            <div class="col-md-3 bubbleContainer">
                <div class="bubble">
                    <span class="attribute">{{$playCountRating}}</span>
                    <br/>
                    <span class="description">Play Count Ranking</span>
                </div>
            </div>
            <div class="col-md-3 bubbleContainer">
                <div class="bubble">
                    <span class="attribute">{{$trophies->count()}}</span>
                    <br/>
                    <span class="description">Trophy Count</span>
                </div>
            </div>
            <div class="col-md-3 bubbleContainer">
                <div class="bubble">
                    <span class="attribute">{{$trophyRating}}</span>
                    <br/>
                    <span class="description">Trophy Ranking</span>
                </div>
            </div>
        </div>
        <div id="profileTopContainer" class="col-md-12">
            <div id="userTrophies" class="col-md-12 profileBox">
                <h2>Trophies</h2>
                @foreach($trophies as $trophy)
                    <div class="trophyContainerLarge" title="{{$trophy->description}}">
                        <img class="trophyImageLarge"  alt="{{$trophy->name}}" src="{!! $trophy->image->src !!}" >
                    </div>
                @endforeach
                @if($trophies->isEmpty())
                    <p>No trophies yet</p>
                @endif
            </div>
        </div>
        <div id="userCharts" class="col-md-12 profileBox">
            <div class="col-md-10 col-md-offset-1 graphContainer">
                <canvas id="percentileGraph"></canvas>
            </div>
        </div>
        <script>
            const gameNames = {!! json_encode($gameNames) !!};
            const percentiles = {!! json_encode($gamePercentiles) !!};

            let percentileCanvas = $("#percentileGraph");
            let percentileChart = new Chart(percentileCanvas, {
                type: 'bar',
                data: {
                    labels: gameNames,
                    datasets: [{
                        label: 'Percentile per game',
                        data: percentiles,
                        backgroundColor: 'rgba(255,255,255,.3)',
                        borderColor: 'white',
                        borderWidth: 3
                    }]
                },
                options: {
                    legend: {labels:{fontColor:"white", fontSize: 18}},
                    scales: {
                        yAxes: [{
                            ticks: {
                                fontColor: 'white',
                                beginAtZero:true
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                fontColor: 'white'
                            }
                        }]
                    },
                    responsive: true,
                    scaleFontColor: "#FFFFFF",
                    pointLabelFontColor: "#FFFFFF"
                }
            });
        </script>
    </div>
@endsection