@extends('layouts.app')
@section('title') What to Watch @endsection
@section('content')
    <h1 id="whatToDo" style="text-align: center; font-size: 72px; margin-top: 80px; margin-bottom: 80px;">...</h1>
    <script>
        var subject = '';
        var minutes = 0;
        var subjects = ['Mandarin', 'F1', 'Classical Music', 'the Ottoman Empire', 'Game Design Theory', 'Graphic Design',
            'Programming Practices', 'the Roman Empire', 'Greece', 'Thailand', 'Samoa', 'the Baroque Period', 'Mongolia', 'England',
            'Denmark', 'Cooking', 'Online Marketing', 'Cryptocurrencies', 'Stock Exchange', 'a 1700\'s US President',
            'a 1800\'s US President', 'a 1900\'s US President', 'the Great Depression', 'How Speakers Work', 'Internal Combustion Engines',
            'Primitive Technology', 'How a Car Works', 'Soap', 'Running a Business', 'Origami', 'Making Chinese Food', 'Math',
            'Music Theory', 'How Cassettes/CD\'s/Records work', 'Paper', 'Japanese History', 'Korean History', 'Calligraphy',
            'Typography', 'Brain Teasers', 'Logic Puzzles', 'Puzzles', 'Nihilism', 'Existentialism', 'Descartes', 'Nietzsche',
            'Napoleon', 'a King George', 'a King Henry', 'a King Louis', 'The Cuban Missile Crisis', 'the USSR', 'East/West Germany',
            'Croatia', 'Modern Italian Government', 'The California Senate', 'The California House', 'Buying Land', 'Real Estate',
            'Building the Foundation of a House', 'Building the Walls of a House', 'Building the Roof of a House', 'Making Furniture',
            'Growing Flowers', 'Growing Food', 'Raising Farm Animals', 'Circuitry', 'Brushless Motors', 'Brushed Motors',
            'Servos', 'Ronald Reagan', 'JFK', 'Nixon', 'How to make a Comic Strip', 'C++', 'The US Navy', 'Tanks', 'How to Write a Haiku',
            'How to Build a Dugout', 'How to Build a Shelter', 'How to Start a Fire', 'Number Theory', 'A Mineral of Your Choice',
            'LEDs', 'CRT Screens', 'Glass', 'Horse Racing', 'Curling', 'The Hammer Throw', 'Archery', 'Skiing', 'Snowboarding',
            'Rugby', 'Martin Luther', 'Jonathan Edwards', 'Charles Spurgeon', 'Cricket', 'Snookers', 'Billiards', 'Tennis',
            'Wrestling', 'Boxing', 'Self Defense', 'Bear Attacks', 'Shark Attacks', 'Karate', 'Jiu-Jitsu', 'Sumo Wrestling',
            'Fencing', 'Swordfighting', 'Coffee', 'Fishing', 'Rock Climbing'];
        var minuteOptions = [3,4,5,5,5,5,5,6,7,8,9,10,3,4,5,6,7,8,9,10,15];
        subject = subjects[Math.floor(Math.random() * subjects.length)];
        minutes = minuteOptions[Math.floor(Math.random() * minuteOptions.length)];
        $("#whatToDo").html('Watch videos on <span style="color: black">' + subject + '</span> for <span style="color: black">' + minutes + '</span> minutes');
    </script>
@endsection