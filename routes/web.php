<?php

use App\Game;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index');

Route::get('/about', function(){
    return view('about');
});

Route::get('/contact', function(){
    return view('contact');
});

Route::get('/faq', function(){
    return view('faq');
});

Route::get('/partners', function(){
    return view('partners');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('game/{slug}', 'GameController@show');

Route::get('play/{slug}', 'GameController@play');

Route::get('game/edit/{slug}', 'GameController@edit');

Route::post('game/update/{slug}', 'GameController@update');

Route::post('game/store', 'GameController@store');

Route::resource('user', 'UserController', ['parameters' => [
    'user' => 'slug'
]]);

Route::post('/rate', 'GameController@rate');

Route::get('admin', 'AdminController@index');

Route::get('admin/createGame', 'AdminController@createGame');

Route::get('admin/updateGame', 'AdminController@updateGame');

Route::get('admin/createTrophy', 'AdminController@createTrophy');

Route::post('trophy/store', 'TrophyController@store');

Route::get('admin/updateTrophy', 'AdminController@updateTrophy');

Route::get('admin/stats/{count}/{unit}', 'AdminController@viewMetrics');
Route::post('admin/switch', 'AdminController@switchView');
Route::get('admin/stats', function(){
    return redirect('admin/stats/7/day');
});

Route::get('admin/removeUser', 'AdminController@removeUser');

Route::get('admin/notifyUsers', 'AdminController@notifyUsers');

Route::middleware('cors')->post('play/{slug}/score/update', 'ScoreController@update');
Route::middleware('cors')->post('play/{slug}/score/store','ScoreController@store');
Route::middleware('cors')->post('play/{slug}/score/create', 'ScoreController@create');
Route::middleware('cors')->post('play/{slug}/trophy/add', 'ScoreController@addTrophy');
Route::middleware('cors')->post('remotePlay', 'ScoreController@remotePlay');

Route::get('usernameValid', 'Auth\RegisterController@usernameValid');
Route::get('emailValid', 'Auth\RegisterController@emailValid');
Route::get('passwordValid', 'Auth\RegisterController@passwordValid');

Route::post('buttonClick/{slug}', 'AdminController@registerButtonClick');

//Route::get('tryToSendMail', 'AdminController@sendTestEmail');
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');
Route::get('{token}/unsubscribe/', 'UserController@unsubscribe');
//Route::get('/resendEmail/{email}', 'Auth\RegisterController@resendEmail');
Route::post('sendContact', 'AdminController@contact');
Route::get('contactSent', function(){
    return view('contactSent');
});
Route::get('whatToWatch', function(){
    return view('whatToWatch');
});
Route::get('leaderBoards', 'GameController@showLeaderboards');

Route::get('test', function(){
    var_dump(\Illuminate\Support\Facades\Auth::user()->currentIP());
});

Route::get('phpInfo', function(){
    phpinfo();
});

Route::middleware('waldock')->get('waldock', 'GotchaController@index');
Route::middleware('waldock')->post('waldock/createPlayer', 'GotchaController@create');
Route::middleware('waldock')->post('waldock/updatePlayer/{id}', 'GotchaController@update');
Route::middleware('waldock')->post('waldock/killPlayer', 'GotchaController@killPlayer');
Route::middleware('waldock')->get('waldock/editPlayer/{id}', 'GotchaController@edit');
Route::middleware('waldock')->get('waldock/choosePlayer', 'GotchaController@choosePlayer');
Route::middleware('waldock')->get('waldock/newPlayer', 'GotchaController@newPlayer');
Route::middleware('waldock')->post('waldock/selectPlayer', 'GotchaController@selectPlayer');
Route::middleware('waldock')->post('waldock/addComment', 'GotchaController@addComment');
Route::get('waldock/login', 'GotchaController@login');
Route::post('waldock/postLogin', 'GotchaController@postLogin');
Route::get('lunch', 'CafController@lunch');
Route::get('breakfast', 'CafController@breakfast');
Route::get('dinner', 'CafController@dinner');
Route::get('supper', 'CafController@dinner');
Route::get('brunch', 'CafController@brunch');
Route::get('meal', 'CafController@makeAMeal');
Route::get('food', 'CafController@random');
Route::get('nextClass', 'CafController@nextClass');
Route::get('tmu', function(){
    return view('shortcuts');
});

Route::get('esther', function(){
    return view('esther');
});